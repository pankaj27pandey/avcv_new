package com.offshoot.avcv.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.apzone.android.common.CommonUtils;
import com.offshoot.avcv.R;
import com.offshoot.avcv.activity.CaseDetailsActivity;
import com.offshoot.avcv.bean.CaseItem;
import com.offshoot.avcv.db.DbHandler;
import com.offshoot.avcv.listadapters.CaseListBaseAdapter;

import java.util.ArrayList;
import java.util.List;

public class CollectionAllocationFragment extends Fragment {


    private OnFragmentInteractionListener mListener;

    private Context mContext;
    private List<CaseItem> caseList;
    private ArrayList<CaseItem> caseListArray;
    private CaseListBaseAdapter caseListBaseAdapter;
    private ListView downloadedListView;
    private CommonUtils utils;
    private DbHandler db;

    public CollectionAllocationFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (mListener != null) {
            mListener.onFragmentInteraction("Collection Allocation");
        }
        mContext = getActivity();
        utils = new CommonUtils(mContext);
        db = new DbHandler(mContext);
        View view = inflater.inflate(R.layout.fragment_collection_cases, container, false);
        downloadedListView = view.findViewById(R.id.downloadedCaseList);
        loadAndShowData();
        downloadedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                CaseItem mItem = caseListArray.get(position);
                Intent mIntent = new Intent(mContext, CaseDetailsActivity.class);
                mIntent.putExtra("VerificationID", mItem.getAVCVDetailsID());
                startActivityForResult(mIntent, 0);
            }
        });

        return view;
    }


    private void loadAndShowData() {
        caseList = db.getAllCaseList(utils.getStringData("LoginId"),"C");
        caseListArray = new ArrayList<CaseItem>();
        for (int i = 0; i < caseList.size(); i++) {
            CaseItem mItem = new CaseItem();
            mItem.setAVCVDetailsID(caseList.get(i).getAVCVDetailsID());
            mItem.setMobileNo(caseList.get(i).getMobileNo());
            mItem.setCustomerName(caseList.get(i).getCustomerName());
            mItem.setAlternateNo(caseList.get(i).getAlternateNo());
            mItem.setAVAddress(caseList.get(i).getAVAddress());
            mItem.setAVLandmark(caseList.get(i).getAVLandmark());
            mItem.setAVCity(caseList.get(i).getAVCity());
            mItem.setAVDistrict(caseList.get(i).getAVDistrict());
            mItem.setAVPINCode(caseList.get(i).getAVPINCode());
            caseListArray.add(mItem);
        }
        if (caseListArray.size() == 0) {
            utils.showCenterToast("No Pending AVCV cases :)");
        }
        caseListBaseAdapter = new CaseListBaseAdapter(mContext, caseListArray);
        downloadedListView.setAdapter(caseListBaseAdapter);
        caseListBaseAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        loadAndShowData();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title);
    }
}
