package com.offshoot.avcv.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.offshoot.avcv.R;
import com.offshoot.avcv.db.DbHandler;
import com.offshoot.avcv.bean.CaseItem;
import com.offshoot.avcv.listadapters.CaseListBaseAdapter;
import com.offshoot.avcv.mconfig.MConfig;
import com.offshoot.avcv.parser.MParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;


public class AllocatedCaseFragment extends Fragment  implements NetworkTaskCompleteListener {

    private OnFragmentInteractionListener mListener;


    Context mContext;
    ArrayList<CaseItem> caseListArray;
    CaseListBaseAdapter caseListBaseAdapter;
    ListView allocatedListView;
    CommonUtils utils;
    DbHandler db;

    public AllocatedCaseFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mListener != null) {
            mListener.onFragmentInteraction("Allocated Case");
        }
        mContext =getActivity();
        utils = new CommonUtils(mContext);
        db = new DbHandler(mContext);

        View view = inflater.inflate(R.layout.fragment_allocated_cases, container, false);

        allocatedListView = view.findViewById(R.id.allocatedCaseList);
        if (utils.isNetworkAvailable(mContext)) {
            Hashtable<String, String> ht = new Hashtable<String, String>();
            String[] keys = {"FECode"};
            String[] values = {utils.getStringData("LoginId")};
            for (int i = 0; i < keys.length; i++) {
                ht.put(keys[i], values[i]);
            }
            new NetworkTask(this, ht, true).execute(Urls.allocatedCaseUrl);
            ht = null;
        } else {
            utils.showCenterToast(MConfig.internetMessage);
        }

        return view;
    }

    void showDataInList() {
        if (caseListArray.size() == 0) {
            utils.showCenterToast("No new case allocated to you :)");
        } else {
            utils.showCenterToast(caseListArray.size()
                    + " new case allocated to you :)");
        }
        caseListBaseAdapter = new CaseListBaseAdapter(mContext, caseListArray);
        allocatedListView.setAdapter(caseListBaseAdapter);
        caseListBaseAdapter.notifyDataSetChanged();
    }

 private    void parseResultData(String jArray) {
        caseListArray = new ArrayList<CaseItem>();
        JSONObject jsonObject = null;
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(jArray);
            for (int index = 0; index < jsonArray.length(); index++) {
                jsonObject = jsonArray.getJSONObject(index);

                CaseItem item_details = new CaseItem();
                if (jsonObject.has("CaseDetailsID")) {
                    item_details.setAVCVDetailsID(jsonObject
                            .getString("CaseDetailsID"));
                }
                if (jsonObject.has("MobileNo")) {
                    item_details.setMobileNo(jsonObject.getString("MobileNo"));
                }
                if (jsonObject.has("CustomerName")) {
                    item_details.setCustomerName(jsonObject
                            .getString("CustomerName"));
                }
                if (jsonObject.has("AlternateNo")) {
                    item_details.setAlternateNo(jsonObject
                            .getString("AlternateNo"));
                }
                if (jsonObject.has("Address")) {
                    item_details
                            .setAVAddress(jsonObject.getString("Address"));
                }
                if (jsonObject.has("Landmark")) {
                    item_details.setAVLandmark(jsonObject
                            .getString("Landmark"));
                }
                if (jsonObject.has("City")) {
                    item_details.setAVCity(jsonObject.getString("City"));
                }
                if (jsonObject.has("District")) {
                    item_details.setAVDistrict(jsonObject
                            .getString("District"));
                }
                if (jsonObject.has("ZipCode")) {
                    item_details
                            .setAVPINCode(jsonObject.getString("ZipCode"));
                }
                if (jsonObject.has("ProductType")) {
                    item_details.setAVCVType(jsonObject.getString("ProductType"));
                }
                if (jsonObject.has("CycleName")) {
                    item_details
                            .setCycleName(jsonObject.getString("CycleName"));
                }
                if (jsonObject.has("CPUDate")) {
                    item_details
                            .setCPUDate(jsonObject.getString("CPUDate"));
                }
                if (jsonObject.has("PickupBalance")) {
                    item_details
                            .setPickupBalance(jsonObject.getString("PickupBalance"));
                }
                if (jsonObject.has("ReferenceNo")) {
                    item_details
                            .setReferenceNo(jsonObject.getString("ReferenceNo"));
                }
                if (jsonObject.has("CompanyID")) {
                    item_details
                            .setCompanyID(jsonObject.getString("CompanyID"));
                }
                if (jsonObject.has("AddressType")) {
                    item_details
                            .setAddressType(jsonObject.getString("AddressType"));
                }
                if (jsonObject.has("DataType")) {
                    item_details
                            .setDataType(jsonObject.getString("DataType"));
                }
                item_details.setUserId(utils.getStringData("LoginId"));
                String mAVCVId = jsonObject.getString("CaseDetailsID");
                if (!db.hasCaseExist(mAVCVId)) {
                    if (!db.hasCaseExistInBackUp(mAVCVId)) {
                        caseListArray.add(item_details);
                        db.insertIntoAllocted(item_details);
                    }
                }
            }
        } catch (Exception ee) {

        }
        showDataInList();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    @Override
    public void onNetworkComplete(String result, String type) {
        String[] resultArray = new MParser(mContext).parseXMLResponse(result);
        if (Boolean.parseBoolean(resultArray[0])) {
            parseResultData(resultArray[1]);
        } else {
            utils.showCenterToast(resultArray[1]);
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String title);
    }
}
