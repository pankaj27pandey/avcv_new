package com.offshoot.avcv.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.offshoot.avcv.R;

public class HelpFragment extends Fragment {

    // NOTE: Removed Some unwanted Boiler Plate Codes
    private OnFragmentInteractionListener mListener;

    public HelpFragment() {
    }

    TextView tvMail, tvPhone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_help, container, false);

        // NOTE : We are calling the onFragmentInteraction() declared in the MainActivity
        // ie we are sending "Fragment 1" as title parameter when fragment1 is activated
        if (mListener != null) {
            mListener.onFragmentInteraction("Help");
        }

       /* view.findViewById(R.id.tvMail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        tvPhone = view.findViewById(R.id.tvPhone);
        tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri u = Uri.parse("tel:" + tvPhone.getText().toString());

                Intent i = new Intent(Intent.ACTION_DIAL, u);

                try {

                    startActivity(i);
                } catch (SecurityException s) {
                    // show() method display the toast with
                    // exception message.
                    Toast.makeText(getActivity(), s.getMessage(), Toast.LENGTH_LONG).show();


            }
        }
    });*/
    // Here we will can create click listners etc for all the gui elements on the fragment.
    // For eg: Button btn1= (Button) view.findViewById(R.id.frag1_btn1);
    // btn1.setOnclickListener(...

        return view;
}


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            // NOTE: This is the part that usually gives you the error
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


public interface OnFragmentInteractionListener {
    // NOTE : We changed the Uri to String.
    void onFragmentInteraction(String title);
}
}
