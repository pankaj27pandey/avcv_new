package com.offshoot.avcv.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.fragment.app.Fragment;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.location.AddressTask;
import com.apzone.android.location.AddressTaskCompleteListener;
import com.apzone.android.location.GPSTracker;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.offshoot.avcv.R;
import com.offshoot.avcv.parser.MParser;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;

import static com.offshoot.avcv.mconfig.MConfig.internetMessage;


public class SalesFragment extends Fragment implements AddressTaskCompleteListener, NetworkTaskCompleteListener {

    private OnFragmentInteractionListener mListener;
    Spinner visitTypeSpinner, followUpSpinner;
    EditText etDateTime, etCompanyName, etPersonMeetWith, etMobile, etDesignation, etRemark;
    TextView locationAddress;
    Button submitButton;
    Context mContext;
    private CommonUtils utils;
    String[] enteredValues;
    String visitTypeStr, remarkStr, followUpStr, loginId, followupDateStr = "", companyNameStr, personMeetStr, mobileNumberStr, designationStr, mLocationAddress;
    GPSTracker tracker;
    Calendar c;
    Fragment fragment;

    String[] checkEnteredData() {
        String[] mValues = {loginId,
                followUpStr, visitTypeStr, companyNameStr, personMeetStr, mobileNumberStr, designationStr, followupDateStr, remarkStr,

                tracker.getLatitude() + ", " + tracker.getLongitude(), mLocationAddress,
        };

        return mValues;
    }

    public SalesFragment() {
        // Required empty public constructor
        tracker = GPSTracker.getInstance();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mListener != null) {
            mListener.onFragmentInteraction("Sales");
        }
        mContext = getActivity();
        fragment = this;
        utils = new CommonUtils(mContext);
        loginId = utils.getStringData("LoginId");
        c = Calendar.getInstance();
        View view = inflater.inflate(R.layout.fragment_sales, container, false);
        defineVariable(view);
        return view;
    }

    void defineVariable(View view) {
        visitTypeSpinner = view.findViewById(R.id.visitTypeSpinner);
        followUpSpinner = view.findViewById(R.id.followUpSpinner);
        etDateTime = view.findViewById(R.id.etDateTime);
        locationAddress = view.findViewById(R.id.locationAddress);
        etCompanyName = view.findViewById(R.id.etCompanyName);
        etDesignation = view.findViewById(R.id.etDesignation);
        etMobile = view.findViewById(R.id.etMobile);
        etPersonMeetWith = view.findViewById(R.id.etPersonMeetWith);
        etRemark = view.findViewById(R.id.etRemark);
        submitButton = view.findViewById(R.id.submitButton);

        mLocationAddress = tracker.getMyAddress();
        locationAddress.setText(tracker.getLatitude() + ", "
                + tracker.getLongitude() + "\n" + mLocationAddress);
        addEventListeners();
    }

    @Override
    public void onAddressComplete(String address) {
        // TODO Auto-generated method stub
        if (address != null) {
            mLocationAddress = address;
            locationAddress.setText(tracker.getLatitude() + ", "
                    + tracker.getLongitude() + "\n" + mLocationAddress);
        }
    }

    void addEventListeners() {

        etDateTime.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDateTimePicker();
                //onCreateDialog(0).show();
            }
        });

        visitTypeSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View mView,
                                               int position, long arg3) {
//
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });

        followUpSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View mView,
                                               int position, long arg3) {
                        String selectedItem = parent
                                .getItemAtPosition(position).toString();
                        if (position == 3 || position == 4) {
                            etDateTime.setVisibility(View.VISIBLE);
                        } else {
                            etDateTime.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });


        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (validate()) {
                    enteredValues = checkEnteredData();
                    if (enteredValues != null) {
                        if (utils.isNetworkAvailable(mContext)) {
                            Hashtable<String, String> ht = new Hashtable<String, String>();
                            String[] keys = Urls.salesFormkeys;
                            for (int i = 0; i < keys.length; i++) {
                                ht.put(keys[i], enteredValues[i] + "");
                            }
                            new NetworkTask(fragment, ht, true)
                                    .execute(Urls.submitSalesForm);
                            ht = null;
                        } else {

                            utils.showCenterToast(internetMessage);
                        }
                    } else {
                        utils.showCenterToast("Please enter all the required values!!");
                    }
                }
            }
        });
    }

    boolean checkGPSStatus = true;

    void checkForGPS() {
        tracker.getLocation(getActivity());
        if (tracker.getLatitude() == 0.0) {
            if (utils.isNetworkAvailable(mContext)) {
                tracker.showSettingsAlert("Error",
                        "Please check your GPS and Internet Settings :)");
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get longitude and latitude due to internet connection !!");
            }
        } else {
            if (utils.isNetworkAvailable(mContext)) {
                new AddressTask(getActivity()).execute(tracker.getLatitude(),
                        tracker.getLongitude());
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get location address due to internet connection !!");
            }
        }
        checkGPSStatus = false;
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        checkForGPS();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }


    private Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                DatePickerDialog dpd = new DatePickerDialog(mContext,
                        pickerListener, c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dpd.setTitle("Month and Year");
                try {

                    Field[] datePickerDialogFields = dpd.getClass()
                            .getDeclaredFields();
                    for (Field datePickerDialogField : datePickerDialogFields) {
                        if (datePickerDialogField.getName().equals("mDatePicker")) {
                            datePickerDialogField.setAccessible(true);
                            DatePicker datePicker = (DatePicker) datePickerDialogField
                                    .get(dpd);
                            Field datePickerFields[] = datePickerDialogField
                                    .getType().getDeclaredFields();
                            for (Field datePickerField : datePickerFields) {
                                if ("mDayPicker".equals(datePickerField.getName())
                                        || "mDaySpinner".equals(datePickerField
                                        .getName())) {
                                    datePickerField.setAccessible(true);
                                    Object dayPicker = new Object();
                                    dayPicker = datePickerField.get(datePicker);
                                    ((View) dayPicker).setVisibility(View.GONE);
                                }
                            }
                        }

                    }
                } catch (Exception ex) {
                }
                return dpd;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub

            // Show selected date
            StringBuilder selectedDate = new StringBuilder()
                    .append(dayOfMonth + 1).append("-").append(monthOfYear + 1).append("-").append(year)
                    .append(" ");

            etDateTime.setText(selectedDate);

        }

    };


    public void showDateTimePicker() {
        final Calendar currentDate = Calendar.getInstance();
        c = Calendar.getInstance();
        new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                c.set(year, monthOfYear, dayOfMonth);
                new TimePickerDialog(mContext, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        c.set(Calendar.MINUTE, minute);
                        Log.v("SaleTime", "The choosen one " + c.getTime());
                        SimpleDateFormat format1 = new SimpleDateFormat("EEE LLL dd HH:mm:ss Z yyyy");
                        SimpleDateFormat format2 = new SimpleDateFormat("ddMMyyyy HHmm");
                        try {
                            Date updateLast = format1.parse(c.getTime().toString());

                            etDateTime.setText(format2.format(updateLast).toString());
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }, currentDate.get(Calendar.HOUR_OF_DAY), currentDate.get(Calendar.MINUTE), false).show();
            }
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH), currentDate.get(Calendar.DATE)).show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        if (tracker != null) {
            tracker.stopUsingGPS();
        }
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();


    }

    void  clearForm(){
       etRemark.getText().clear();
       etDateTime.getText().clear();
       etMobile.getText().clear();
       etPersonMeetWith.getText().clear();
       etCompanyName.getText().clear();
       etDesignation.getText().clear();
       visitTypeSpinner.setSelection(0);
       followUpSpinner.setSelection(0);
    }

    @Override
    public void onNetworkComplete(String result, String type) {
        String[] resultArray = new MParser(mContext).parseXMLResponse(result);
        if (Boolean.parseBoolean(resultArray[0])) {
            clearForm();

        }

        utils.showCenterToast(resultArray[1]);

    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String title);
    }


    private boolean validate() {
        boolean isValid = true;
        if (visitTypeSpinner.isShown()) {
            if (visitTypeSpinner.getSelectedItem().toString() == null
                    || visitTypeSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Visit Type ");
                isValid = false;
            } else
                visitTypeStr = visitTypeSpinner.getSelectedItem().toString();
        } else {

            visitTypeStr = "";
        }


        if (followUpSpinner.isShown()) {
            if (followUpSpinner.getSelectedItem().toString() == null
                    || followUpSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Follow up status ");
                isValid = false;
            } else
                followUpStr = followUpSpinner.getSelectedItem().toString();
        } else {

            followUpStr = "";
        }

//

        if (etCompanyName.isShown()) {
            if (etCompanyName.getText().toString() == null
                    || etCompanyName.getText().toString().length() <= 0
                    || etCompanyName.getText().toString() == "") {
                utils.showCenterToast("Please enter Company name ");
                isValid = false;
            } else
                companyNameStr = etCompanyName.getText().toString();
        } else {
            companyNameStr = "";
        }


        if (etPersonMeetWith.isShown()) {
            if (etPersonMeetWith.getText().toString() == null
                    || etPersonMeetWith.getText().toString().length() <= 0
                    || etPersonMeetWith.getText().toString() == "") {
                utils.showCenterToast("Please enter Person Meet with ");
                isValid = false;
            } else
                personMeetStr = etPersonMeetWith.getText().toString();
        } else {
            personMeetStr = "";
        }

        if (etMobile.isShown()) {
            if (etMobile.getText().toString() == null
                    || etMobile.getText().toString().length() <= 0
                    || etMobile.getText().toString() == "") {
                utils.showCenterToast("Please enter mobile number ");
                isValid = false;
            } else
                mobileNumberStr = etMobile.getText().toString();
        } else {
            mobileNumberStr = "";

        }

        if (etDesignation.isShown()) {
            if (etDesignation.getText().toString() == null
                    || etDesignation.getText().toString().length() <= 0
                    || etDesignation.getText().toString() == "") {
                utils.showCenterToast("Please enter Designation ");
                isValid = false;
            } else
                designationStr = etDesignation.getText().toString();
        } else {
            designationStr = "";

        }

        if (etDateTime.isShown()) {
            if (etDateTime.getText().toString() == null
                    || etDateTime.getText().toString().length() <= 0
                    || etDateTime.getText().toString() == "") {
                utils.showCenterToast("Please enter Select Follow up date ");
                isValid = false;
            } else
                followupDateStr = etDateTime.getText().toString();
        } else {
            followupDateStr = "";

        }

        if (etRemark.isShown()) {
            if (etDesignation.getText().toString() == null
                    || etRemark.getText().toString().length() <= 0
                    || etRemark.getText().toString() == "") {
                utils.showCenterToast("Please enter remark ");
                isValid = false;
            } else
                remarkStr = etRemark.getText().toString();
        } else {
            remarkStr = "";

        }


        return isValid;
    }
}
