package com.offshoot.avcv.parser;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import android.content.Context;
import android.util.Log;

public class MParser {
	Context mContext;

	public MParser(Context context) {
		mContext = context;
	}
	public String parseJSON(String mJson, String mKey) {
		if (mJson == null) {
			return null;
		}
		try {
			JSONObject jsonResponse = new JSONObject(mJson);
			String value = jsonResponse.getString(mKey);
			if (value == null) {
				return null;
			}
			if (value.equals("null")) {
				return null;
			}
			return value;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	};

	public String xmlToJson(String myxml) {
		JSONObject jsonObj = null;
		try {
			jsonObj = XML.toJSONObject(myxml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (jsonObj == null) {
			return null;
		}
		// return jsonObj.toString();
		return parseJSON(parseJSON(jsonObj.toString(), "string"), "content");
	}

	public String[] splitResponse(String resp) {
		String[] respArray = { "false",
				"Network timeout or something went wrong during prosses, please try again!" };
		try {
			if (resp.indexOf("@") == -1) {
				respArray[0] = "true";
				respArray[1] = resp;
			} else {
				respArray = resp.split("@");
				respArray[0] = respArray[0]
						.substring(respArray[0].length() - 1);
				if (respArray[0].equals("1")) {
					respArray[0] = "true";
				} else {
					respArray[0] = "false";
				}
			}
		} catch (Exception ee) {
			Log.e("XML Parser error", ee + "");
		}
		return respArray;
	}

	public String[] parseXMLResponse(String resp) {
		return splitResponse(xmlToJson(resp));
	}
}
