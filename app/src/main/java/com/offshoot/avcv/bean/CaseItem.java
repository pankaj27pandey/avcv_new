package com.offshoot.avcv.bean;

//"AVCVDetailsID":"811522e8-a85f-406f-af3f-cef03451c952","MobileNo":7878787873,"CustomerName":"ARPIT SHARMA","AlternateNo":7417484696,"AVAddress":"Meerut","AVLandmark":"SK Road","AVCity":"Meerut","AVDistrict":"Meerut","AVPINCode":250001,"AVCVType":"OFFICE","Portfolio":"HERO"

public class CaseItem {
	private String VerificationID;
	private String MobileNo;
	private String CustomerName;
	private String AlternateNo;
	private String AVAddress;
	private String AVLandmark;
	private String AVCity;
	private String AVDistrict;
	private String AVPINCode;
	private String AVCVType;
	private String userId;
	private String MTimeStamp;
	private String CycleName;
	private String CPUDate;
	private String PickupBalance;
	private String ReferenceNo;
	private String CompanyID;
	private String DataType;
	private String AddressType;

	public String getAddressType() {
		return AddressType;
	}

	public void setAddressType(String addressType) {
		AddressType = addressType;
	}


	public String getCycleName() {
		return CycleName;
	}

	public void setCycleName(String cycleName) {
		CycleName = cycleName;
	}

	public String getCPUDate() {
		return CPUDate;
	}

	public void setCPUDate(String CPUDate) {
		this.CPUDate = CPUDate;
	}

	public String getPickupBalance() {
		return PickupBalance;
	}

	public void setPickupBalance(String pickupBalance) {
		PickupBalance = pickupBalance;
	}

	public String getReferenceNo() {
		return ReferenceNo;
	}

	public void setReferenceNo(String referenceNo) {
		ReferenceNo = referenceNo;
	}

	public String getCompanyID() {
		return CompanyID;
	}

	public void setCompanyID(String companyID) {
		CompanyID = companyID;
	}

	public String getDataType() {
		return DataType;
	}

	public void setDataType(String dataType) {
		DataType = dataType;
	}




	public String getAVCVDetailsID() {
		return VerificationID;
	}

	public void setAVCVDetailsID(String value) {
		this.VerificationID = value;
	}

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String value) {
		this.MobileNo = value;
	}

	public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String value) {
		this.CustomerName = value;
	}

	public String getAlternateNo() {
		return AlternateNo;
	}

	public void setAlternateNo(String value) {
		this.AlternateNo = value;
	}

	public String getAVAddress() {
		return AVAddress;
	}

	public void setAVAddress(String value) {
		this.AVAddress = value;
	}

	public String getAVLandmark() {
		return AVLandmark;
	}

	public void setAVLandmark(String value) {
		this.AVLandmark = value;
	}

	public String getAVCity() {
		return AVCity;
	}

	public void setAVCity(String value) {
		this.AVCity = value;
	}

	public String getAVDistrict() {
		return AVDistrict;
	}

	public void setAVDistrict(String value) {
		this.AVDistrict = value;
	}

	public String getAVPINCode() {
		return AVPINCode;
	}

	public void setAVPINCode(String value) {
		this.AVPINCode = value;
	}

	public String getAVCVType() {
		return AVCVType;
	}

	public void setAVCVType(String value) {
		this.AVCVType = value;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String value) {
		this.userId = value;
	}
	public String getMTimeStamp() {
		return MTimeStamp;
	}

	public void setMTimeStamp(String value) {
		this.MTimeStamp = value;
	}

}