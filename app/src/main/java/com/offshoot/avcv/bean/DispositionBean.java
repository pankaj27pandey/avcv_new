package com.offshoot.avcv.bean;



public class DispositionBean {

    private String FE_STATUS;
    private String FE_REASON1;
    private String FE_REASON2;
    private String DATA_TYPE;

    public String getFE_STATUS() {
        return FE_STATUS;
    }

    public void setFE_STATUS(String FE_STATUS) {
        this.FE_STATUS = FE_STATUS;
    }

    public String getFE_REASON1() {
        return FE_REASON1;
    }

    public void setFE_REASON1(String FE_REASON1) {
        this.FE_REASON1 = FE_REASON1;
    }

    public String getFE_REASON2() {
        return FE_REASON2;
    }

    public void setFE_REASON2(String FE_REASON2) {
        this.FE_REASON2 = FE_REASON2;
    }

    public String getDATA_TYPE() {
        return DATA_TYPE;
    }

    public void setDATA_TYPE(String DATA_TYPE) {
        this.DATA_TYPE = DATA_TYPE;
    }
}
