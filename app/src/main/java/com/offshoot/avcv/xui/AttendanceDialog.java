package com.offshoot.avcv.xui;

import java.util.Hashtable;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.common.Validation;
import com.apzone.android.network.NetworkTask;
import com.offshoot.avcv.R;
import com.offshoot.avcv.mconfig.MConfig;

public class AttendanceDialog {
	Context mContext;
	TextView attendanceName, subHeadingText;
	Spinner spinner;
	EditText reason;
	Button submitButton;
	Dialog dialog;
	String userName, userId;
	
	//Create require objects
	CommonUtils utils;

	void definedVariables() {
		attendanceName = (TextView) dialog.findViewById(R.id.attendanceName);
		subHeadingText = (TextView) dialog.findViewById(R.id.subHeadingText);
		spinner = (Spinner) dialog.findViewById(R.id.attendanceSpinner);
		reason = (EditText) dialog.findViewById(R.id.reasonEditText);
		submitButton = (Button) dialog.findViewById(R.id.attendenceSubmit);

		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
			attendanceName.setTextColor(Color.WHITE);
			subHeadingText.setTextColor(Color.WHITE);
		}
	}
	
	String checkEnteredData(){
		if (!new Validation(mContext).isValidField(reason)) {
			return null;
		}
		return reason.getText().toString();
	}
	void mEventListeners(){
		submitButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String attendValue = spinner.getSelectedItem().toString();
				attendValue = attendValue.substring(0, 1).toLowerCase();
				if(attendValue.equals("a")){
					if(checkEnteredData() == null){
						utils.showCenterToast("Reason is required in case of absence!!");
						return;
					}
				}
				if (utils.isNetworkAvailable(mContext)) {
					Hashtable<String, String> ht = new Hashtable<String, String>();
					String[] keys = { "LoginID", "AttendanceStatus" ,"AttendanceReason"};
					String[] values = {userId, attendValue, reason.getText().toString()};
					for (int i = 0; i < keys.length; i++) {
						ht.put(keys[i], values[i]);
					}
					new NetworkTask(mContext, ht, true).execute(Urls.attendanceUrl);
					ht = null;
					keys = null;
					values = null;
				} else {
					utils.showCenterToast(MConfig.internetMessage);
				}
				
			}
		});
	}

	public AttendanceDialog(Context context) {
		mContext = context;
		utils = new CommonUtils(mContext);
		
		dialog = new Dialog(mContext);
		dialog.setContentView(R.layout.dialog_attandence);
		dialog.setTitle("FOS Attendance");
		definedVariables();
		mEventListeners();
	}

	public void show(String uName, String uId) {
		userName  = uName;
		userId = uId;
		attendanceName.setText("Hi " + userName);
		dialog.show();
	}
	
	public void hide(){
		dialog.dismiss();
	}
}
