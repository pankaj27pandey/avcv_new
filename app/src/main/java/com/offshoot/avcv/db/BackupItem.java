package com.offshoot.avcv.db;

public class BackupItem {
	String avcvId;
	String avcvData;
	String mUserId;
	String mType;
	String backupId;
	byte[] tempByteData = "TEMP AVCV DATA".getBytes();
	byte[] mSignImage = tempByteData;
	byte[] mUserImage = tempByteData;
	byte[] mPlaceImage = tempByteData;

	public String getBackupId() {
		return this.backupId;
	}

	public void setBackupId(String value) {
		this.backupId = value;
	}

	public String getUserId() {
		return this.mUserId;
	}

	public void setUserId(String value) {
		this.mUserId = value;
	}

	public String getAvcvId() {
		return this.avcvId;
	}

	public void setAvcvId(String value) {
		this.avcvId = value;
	}

	public String getAvcvData() {
		return this.avcvData;
	}

	public void setAvcvData(String value) {
		this.avcvData = value;
	}

	public String getType() {
		return this.mType;
	}

	public void setType(String value) {
		this.mType = value;
	}

	public byte[] getSignImage() {
		return this.mSignImage;
	};

	public void setSignImage(byte[] value) {
		this.mSignImage = value;
	}

	public byte[] getUserImage() {
		return this.mUserImage;
	};

	public void setUserImage(byte[] value) {
		this.mUserImage = value;
	}

	public byte[] getPlaceImage() {
		return this.mPlaceImage;
	};

	public void setPlaceImage(byte[] value) {
		this.mPlaceImage = value;
	}
}
