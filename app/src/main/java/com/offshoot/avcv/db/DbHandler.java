package com.offshoot.avcv.db;

import java.util.ArrayList;
import java.util.List;

import com.offshoot.avcv.bean.CaseItem;
import com.offshoot.avcv.bean.DispositionBean;
import com.offshoot.avcv.bean.PaymentModeBean;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.apzone.android.common.AppConstants.DB_VERSION;
import static com.apzone.android.common.AppConstants.dbName;

public class DbHandler extends SQLiteOpenHelper {

	Context mContext;



	static final String AllocatedCaseTable = "AllocatedCaseTable";

	static final String MTimeStamp = "MTimeStamp";
	static final String UserId = "UserId";
	static final String AVCVDetailsID = "VerificationID";
	static final String MobileNo = "MobileNo";
	static final String CustomerName = "CustomerName";
	static final String AlternateNo = "AlternateNo";
	static final String AVAddress = "AVAddress";
	static final String AVLandmark = "AVLandmark";
	static final String AVCity = "AVCity";
	static final String AVDistrict = "AVDistrict";
	static final String AVPINCode = "AVPINCode";
	static final String AVCVType = "AVCVType";
	static final String CycleName = "CycleName";
	static final String CPUDate = "CPUDate";
	static final String PickupBalance = "PickupBalance";
	static final String ReferenceNo = "ReferenceNo";
	static final String CompanyID = "CompanyID";
	static final String DataType = "DataType";
	static final  String AddressType="AddressType";

	static final String BackupDataTable = "BackupDataTable";
	static final String AVCVData = "AVCVData";
	static final String SignImage = "SignImage";
	static final String UserImage = "UserImage";
	static final String PlaceImage = "PlaceImage";

	//
	static final String DespositionTable = "DESPOSITION_TABLE";
	static final String FE_STATUS = "FE_STATUS";
	static final String FE_REASON1 = "FE_REASON1";
	static final String FE_REASON2 = "FE_REASON2";
	static final String DATA_TYPE = "DATA_TYPE";

	static final String PaymentModeTable = "PAYMENTMODE_TABLE";
	static final String PaymentMode = "PaymentMode";


	public DbHandler(Context context) {
		super(context,dbName , null, DB_VERSION);
		mContext = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.e("CreateTable", "createTable");
		db.execSQL("CREATE TABLE IF NOT EXISTS " + AllocatedCaseTable + "("
				+ MTimeStamp + " TEXT PRIMARY KEY," + UserId + " Text,"
				+ AVCVDetailsID + " Text," + MobileNo + " Text," + CustomerName
				+ " Text," + AlternateNo + " Text," + AVAddress + " Text,"
				+ AVLandmark + " Text," + AVCity + " Text," + AVDistrict
				+ " Text," + AVPINCode + " Text," + AVCVType + " Text,"
				+ CycleName + " Text,"
				+ CPUDate + " Text,"
				+ PickupBalance + " Text,"
				+ ReferenceNo + " Text,"
				+ CompanyID + " Text,"
				+ AddressType + " Text,"
				+ DataType + " Text);");

		db.execSQL("CREATE TABLE IF NOT EXISTS " + BackupDataTable
				+ "(ID INTEGER PRIMARY KEY AUTOINCREMENT," + UserId + " Text,"
				+ AVCVDetailsID + " Text," + AVCVData + " Text," + AVCVType
				+ " Text," + SignImage + " BLOB," + UserImage + " BLOB,"
				+ PlaceImage + " BLOB);");
		db.execSQL("CREATE TABLE IF NOT EXISTS " + DespositionTable + "("
				+ FE_STATUS + " Text," + FE_REASON1 + " Text,"+ FE_REASON2 + " Text," + DATA_TYPE + " Text);");

		db.execSQL("CREATE TABLE IF NOT EXISTS " + PaymentModeTable + "("
				+ PaymentMode + " Text);");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + AllocatedCaseTable);
		db.execSQL("DROP TABLE IF EXISTS " + BackupDataTable);
		db.execSQL("DROP TABLE IF EXISTS " + DespositionTable);
		db.execSQL("DROP TABLE IF EXISTS " + PaymentModeTable);

		onCreate(db);
	}

	public void insertIntoAllocted(CaseItem mItem) {

		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues cv = new ContentValues();
			cv.put(MTimeStamp, System.currentTimeMillis() + "");
			cv.put(UserId, mItem.getUserId());
			cv.put(AVCVDetailsID, mItem.getAVCVDetailsID());
			cv.put(MobileNo, mItem.getMobileNo());
			cv.put(CustomerName, mItem.getCustomerName());
			cv.put(AlternateNo, mItem.getAlternateNo());
			cv.put(AVAddress, mItem.getAVAddress());
			cv.put(AVLandmark, mItem.getAVLandmark());
			cv.put(AVCity, mItem.getAVCity());
			cv.put(AVDistrict, mItem.getAVDistrict());
			cv.put(AVPINCode, mItem.getAVPINCode());
			cv.put(AVCVType, mItem.getAVCVType());
			cv.put(CycleName, mItem.getCycleName());
			cv.put(CPUDate,mItem.getCPUDate());
			cv.put(PickupBalance,mItem.getPickupBalance());
			cv.put(ReferenceNo,mItem.getReferenceNo());
			cv.put(CompanyID,mItem.getCompanyID());
			cv.put(AddressType,mItem.getAddressType());
			cv.put(DataType,mItem.getDataType());
			db.insert(AllocatedCaseTable, null, cv);

		} catch (Exception ee) {
			Log.e("InsertTable", ee.getMessage());
		}
		db.close();
		db = null;
	}

	public List<CaseItem> getAllCaseList(String userId,String dataType) {

		ArrayList<CaseItem> mList = new ArrayList<CaseItem>();
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + AllocatedCaseTable + " where "
				+ UserId + "='" + userId+ "' AND " + DataType + "='" + dataType + "' ORDER BY " + MTimeStamp + " DESC;";
		Cursor c = db.rawQuery(query, null);
		while (c.moveToNext()) {
			CaseItem mItem = new CaseItem();
			mItem.setMTimeStamp(c.getString(0));
			mItem.setUserId(c.getString(1));
			mItem.setAVCVDetailsID(c.getString(2));
			mItem.setMobileNo(c.getString(3));
			mItem.setCustomerName(c.getString(4));
			mItem.setAlternateNo(c.getString(5));
			mItem.setAVAddress(c.getString(6));
			mItem.setAVLandmark(c.getString(7));
			mItem.setAVCity(c.getString(8));
			mItem.setAVDistrict(c.getString(9));
			mItem.setAVPINCode(c.getString(10));
			mItem.setAVCVType(c.getString(11));
			mItem.setCycleName(c.getString(12));
			mItem.setCPUDate(c.getString(13));
			mItem.setPickupBalance(c.getString(14));
			mItem.setReferenceNo(c.getString(15));
			mItem.setCompanyID(c.getString(16));
			mItem.setAddressType(c.getString(17));
			mItem.setDataType(c.getString(18));

			mList.add(mItem);
		}
		c.close();
		c = null;
		db.close();
		db = null;
		return mList;
	}

	public CaseItem getCasesFromDetailId(String detailsId) {
		CaseItem mItem = null;
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + AllocatedCaseTable + " where "
				+ AVCVDetailsID + "='" + detailsId + "';";
		Cursor c = db.rawQuery(query, null);
		if (c.moveToNext()) {
			mItem = new CaseItem();
			mItem.setMTimeStamp(c.getString(0));
			mItem.setUserId(c.getString(1));
			mItem.setAVCVDetailsID(c.getString(2));
			mItem.setMobileNo(c.getString(3));
			mItem.setCustomerName(c.getString(4));
			mItem.setAlternateNo(c.getString(5));
			mItem.setAVAddress(c.getString(6));
			mItem.setAVLandmark(c.getString(7));
			mItem.setAVCity(c.getString(8));
			mItem.setAVDistrict(c.getString(9));
			mItem.setAVPINCode(c.getString(10));
			mItem.setAVCVType(c.getString(11));
			mItem.setCycleName(c.getString(12));
			mItem.setCPUDate(c.getString(13));
			mItem.setPickupBalance(c.getString(14));
			mItem.setReferenceNo(c.getString(15));
			mItem.setCompanyID(c.getString(16));
			mItem.setAddressType(c.getString(17));
			mItem.setDataType(c.getString(18));
		}
		c.close();
		c = null;
		db.close();
		db = null;
		return mItem;
	}

	public void deleteFromCases(String detailsId) {
		SQLiteDatabase db = this.getWritableDatabase();
		String deleteRestoreData = "DELETE FROM " + AllocatedCaseTable
				+ " where " + AVCVDetailsID + "='" + detailsId + "';";
		db.execSQL(deleteRestoreData);
		db.close();
		db = null;
	}

	public void deleteAllocatedCaseTable() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + AllocatedCaseTable);
		db.close();
		db = null;
	}

	public Boolean hasCaseExist(String detailsId) {
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + AllocatedCaseTable + " where "
				+ AVCVDetailsID + "='" + detailsId + "';";
		Cursor c = db.rawQuery(query, null);
		if (c.moveToNext()) {
			db.close();
			return true;
		}
		db.close();
		return false;
	}

	// ========================================================================
	public void insertIntoBackUp(BackupItem mItem) {

		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues cv = new ContentValues();
			cv.put(UserId, mItem.getUserId());
			cv.put(AVCVDetailsID, mItem.getAvcvId());
			cv.put(AVCVData, mItem.getAvcvData());
			cv.put(AVCVType, mItem.getType());
			cv.put(SignImage, mItem.getSignImage());
			cv.put(UserImage, mItem.getUserImage());
			cv.put(PlaceImage, mItem.getPlaceImage());
			db.insert(BackupDataTable, null, cv);
		} catch (Exception ee) {
			Log.e("InsertBackupTable", ee.getMessage());
		}
		db.close();
		db = null;
	}

	public List<BackupItem> getAllBackupList(String userId) {

		ArrayList<BackupItem> mList = new ArrayList<BackupItem>();
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + BackupDataTable + " where " + UserId
				+ "='" + userId + "' ORDER BY ID ASC;";
		Cursor c = db.rawQuery(query, null);
		while (c.moveToNext()) {
			BackupItem mItem = new BackupItem();
			mItem.setBackupId(c.getInt(0) + "");
			mItem.setUserId(c.getString(1));
			mItem.setAvcvId(c.getString(2));
			mItem.setAvcvData(c.getString(3));
			mItem.setType(c.getString(4));
			mItem.setSignImage(c.getBlob(5));
			mItem.setUserImage(c.getBlob(6));
			mItem.setPlaceImage(c.getBlob(7));
			mList.add(mItem);
		}
		c.close();
		c = null;
		db.close();
		db = null;
		return mList;
	}

	public Boolean hasCaseExistInBackUp(String detailsId) {
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + BackupDataTable + " where "
				+ AVCVDetailsID + "='" + detailsId + "';";
		Cursor c = db.rawQuery(query, null);
		if (c.moveToNext()) {
			db.close();
			return true;
		}
		db.close();
		return false;
	}

	public void deleteFromBackUp(String detailsId) {
		SQLiteDatabase db = this.getWritableDatabase();
		String deleteRestoreData = "DELETE FROM " + BackupDataTable + " where "
				+ AVCVDetailsID + "='" + detailsId + "';";
		db.execSQL(deleteRestoreData);
		db.close();
		db = null;
	}

	public void deleteBackupDataTable() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + BackupDataTable);
		db.close();
		db = null;
	}

	public List<DispositionBean> getFeReason(String feStatus,String dataType) {
		List<DispositionBean> simNoList = new ArrayList<>();
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + DespositionTable + " where "
				+ FE_STATUS + "='" + feStatus +  "' AND " + DATA_TYPE + "='" + dataType + "';";
		Cursor c = db.rawQuery(query, null);

		c.moveToFirst();
		for (int i = 0; i < c.getCount(); i++) {
			DispositionBean feReasonBean = new DispositionBean();
			feReasonBean.setFE_REASON1(c.getString(1));
			feReasonBean.setFE_REASON2(c.getString(2));
			simNoList.add(feReasonBean);

			c.moveToNext();
		}


		c.close();
		c = null;
		db.close();
		db = null;
		return simNoList;
	}

	public List<String> getDisposition(String feStatus,String dataType) {
		List<String> simNoList = new ArrayList<String>();
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + DespositionTable + " where "
				+ FE_STATUS + "='" + feStatus +  "' AND " + DATA_TYPE + "='" + dataType + "';";
		Cursor c = db.rawQuery(query, null);

		c.moveToFirst();
		for (int i = 0; i < c.getCount(); i++) {
			simNoList.add(c.getString(1));
			c.moveToNext();
		}


		c.close();
		c = null;
		db.close();
		db = null;
		return simNoList;
	}


	//Dispositions
	public void insertDisposition(DispositionBean mItem) {

		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues cv = new ContentValues();

			cv.put(FE_STATUS, mItem.getFE_STATUS());
			cv.put(FE_REASON1, mItem.getFE_REASON1());
			cv.put(FE_REASON2, mItem.getFE_REASON2());
			cv.put(DATA_TYPE, mItem.getDATA_TYPE());

			db.insert(DespositionTable, null, cv);
		} catch (Exception ee) {
			Log.e("insertReason", ee.getMessage());
		}
		db.close();
		db = null;
	}
	public void deleteDesposition() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + DespositionTable);
		db.close();
		db = null;
	}

	//PaymentMode
//Dispositions
	public void insertPaymentMode(PaymentModeBean mItem) {

		SQLiteDatabase db = this.getWritableDatabase();
		try {
			ContentValues cv = new ContentValues();

			cv.put(PaymentMode, mItem.getPaymentMode());


			db.insert(PaymentModeTable, null, cv);
		} catch (Exception ee) {
			Log.e("insertReason", ee.getMessage());
		}
		db.close();
		db = null;
	}

	public List<String> getAllPaymentMode() {
		ArrayList<String> mList = new ArrayList<String>();
		SQLiteDatabase db = this.getReadableDatabase();
		String query = "SELECT * FROM " + PaymentModeTable;
		Cursor c = db.rawQuery(query, null);
		while (c.moveToNext()) {
			//PaymentModeBean mItem = new PaymentModeBean();
			// mItem.setPaymentMode(c.getString(0));

			mList.add(c.getString(0));
		}
		c.close();
		c = null;
		db.close();
		db = null;
		return mList;
	}
	public void deletePaymentMode() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DELETE FROM " + PaymentModeTable);
		db.close();
		db = null;
	}

}