package com.offshoot.avcv.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.common.Validation;
import com.apzone.android.common.XUI;
import com.apzone.android.crop.Crop;
import com.apzone.android.location.AddressTask;
import com.apzone.android.location.AddressTaskCompleteListener;
import com.apzone.android.location.GPSTracker;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.offshoot.avcv.R;
import com.offshoot.avcv.db.BackupItem;
import com.offshoot.avcv.db.DbHandler;
import com.offshoot.avcv.mconfig.MConfig;
import com.offshoot.avcv.parser.MParser;

import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

import static com.apzone.android.common.AppConstants.unTraceType;

public class UntraceableActivity extends AppCompatActivity implements
        AddressTaskCompleteListener, NetworkTaskCompleteListener {
    ImageView navbarBackIcon;
    LinearLayout navbarBack;
    TextView title;



    void definedAndSetUI() {
        navbarBackIcon = (ImageView) findViewById(R.id.navbarBackIcon);
        navbarBack = (LinearLayout) findViewById(R.id.navbarBack);
        title = (TextView) findViewById(R.id.navbarTitle);
        title.setText("Untraceable Form");
        navbarBackIcon.setVisibility(View.VISIBLE);
        navbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

    Context mContext = UntraceableActivity.this;
    CommonUtils utils = new CommonUtils(mContext);
    XUI xui = new XUI(mContext);
    DbHandler db = new DbHandler(mContext);
    GPSTracker tracker = GPSTracker.getInstance();
    Calendar c = Calendar.getInstance();
    Validation valid;

    TextView locationAddress;
    ImageView customerImage, houseImage;
    Button submitButton;

    String AVCVDetailsID, UnTracedReason,feRemark_str, mLocationAddress, customerImageBytes,
            houseImageBytes, LatitudeLongitude;
    byte[] userImageBlobData = null;
    byte[] placeImageBlobData = null;
    int imageType = 1;
    EditText tc_remarksEditText;
    Spinner reasonSpinner;
    List<String> dispositionList;
    ArrayAdapter<String> adapter;

    String[] checkEnteredData() {

        if (!valid.isValidImage(customerImageBytes, "Customer Image")
                || !valid.isValidImage(houseImageBytes, "House Image")) {
            return null;
        }
        String[] mValues = {utils.getStringData("LoginId"), AVCVDetailsID, UnTracedReason, LatitudeLongitude,
                mLocationAddress, customerImageBytes, houseImageBytes};
        return mValues;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_untracable);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Bundle mBundel = getIntent().getExtras();
        AVCVDetailsID = mBundel.getString("VerificationID");
        UnTracedReason = mBundel.getString("UnTracedReason");
        definedAndSetUI();
        defiendVaribles();
        addEventListeners();
        valid = new Validation(this);

    }

    void defiendVaribles() {
        locationAddress =findViewById(R.id.locationAddress);
        reasonSpinner=findViewById(R.id.reasonSpinner);
        tc_remarksEditText=findViewById(R.id.tc_remarksEditText);
        customerImage =  findViewById(R.id.customerImage);
        houseImage =  findViewById(R.id.houseImage);
        submitButton =findViewById(R.id.submitButton);

        dispositionList = db.getDisposition(unTraceType,"COL");
        dispositionList.add(0, "Select");
        adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, dispositionList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reasonSpinner.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    void addEventListeners() {
        customerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imageType = 1;
                xui.createImagePickOptionDialog();

            }
        });
        houseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imageType = 2;
                xui.createImagePickOptionDialog();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (validate()) {
                    String[] enteredValues = checkEnteredData();
                    if (enteredValues != null) {
                        Hashtable<String, String> ht = new Hashtable<String, String>();
                        String[] keys = Urls.untraceableFormkeys;
                        for (int i = 0; i < keys.length; i++) {
                            ht.put(keys[i], enteredValues[i] + "");
                        }
                        if (utils.isNetworkAvailable(mContext)) {
                            new NetworkTask(mContext, ht, true)
                                    .execute(Urls.submitUntraceableUrl);
                        } else {
                            try {

                                int mLength = enteredValues.length;
                                enteredValues[mLength - 2] = "SavedUserImage";
                                enteredValues[mLength - 1] = "SavedPlaceImage";
                                BackupItem mItem = new BackupItem();
                                mItem.setUserId(utils.getStringData("LoginId"));
                                mItem.setAvcvId(AVCVDetailsID);
                                mItem.setAvcvData(utils
                                        .convertArrayToString(enteredValues));
                                mItem.setType("untrace");
                                mItem.setUserImage(userImageBlobData);
                                mItem.setPlaceImage(placeImageBlobData);
                                db.insertIntoBackUp(mItem);
                                db.deleteFromCases(AVCVDetailsID);
                                finish();
                                utils.showCenterToast(MConfig.internetMessage
                                        + "\nYour data has been saved in local storage!!");
                            } catch (Exception ee) {
                                utils.showCenterToast("Unable to save data"
                                        + "\n" + ee.getMessage());
                            }
                        }
                        ht = null;
                    }
                }
            }
        });
    }


    boolean checkGPSStatus = true;

    void checkForGPS() {
        tracker.getLocation(this);
        if (tracker.getLatitude() == 0.0) {
            if (utils.isNetworkAvailable(mContext)) {
                tracker.showSettingsAlert("Error",
                        "Please check your GPS and Internet Settings :)");
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get longitude and latitude due to internet connection !!");
            }
        } else {
            if (utils.isNetworkAvailable(mContext)) {
                new AddressTask(this).execute(tracker.getLatitude(),
                        tracker.getLongitude());
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get location address due to internet connection !!");
            }
        }
        checkGPSStatus = false;
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        // you can use this in "if" to test if location is updated ok
        checkForGPS();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent result) {
        super.onActivityResult(requestCode, resultCode, result);
        byte[] imageArray = Crop.getImageBitmap(requestCode, resultCode,
                result, (Activity) mContext, false);
        if (imageArray != null) {
            ImageView resultImageView = null;
            String base64StringImage = Base64.encodeToString(imageArray, 0);
            switch (imageType) {
                case 1:
                    userImageBlobData = imageArray;
                    resultImageView = customerImage;
                    customerImageBytes = base64StringImage;
                    break;
                case 2:
                    placeImageBlobData = imageArray;
                    resultImageView = houseImage;
                    houseImageBytes = base64StringImage;
                    break;
            }

            resultImageView.setImageDrawable(null);
            Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0,
                    imageArray.length);
            resultImageView.setImageBitmap(bmp);
//			resultImageView.setImageDrawable(null);
//			Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0,
//					imageArray.length);
//			Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
//
//
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system
//
//			Canvas cs = new Canvas(mutableBitmap);
//			Paint tPaint = new Paint();
//			tPaint.setTextSize(35);
//			tPaint.setColor(Color.RED);
//			tPaint.setStyle(Paint.Style.FILL);
//			cs.drawBitmap(mutableBitmap, 0f, 0f, null);
//			float height = tPaint.measureText("yY");
//			cs.drawText(dateTime, 20f, height + 15f, tPaint);
//			resultImageView.setImageBitmap(mutableBitmap);
        }
    }

    @Override
    public void onNetworkComplete(String result, String type) {
        // TODO Auto-generated method stub
        String[] resultArray = new MParser(mContext).parseXMLResponse(result);
        if (Boolean.parseBoolean(resultArray[0])) {
            db.deleteFromCases(AVCVDetailsID);
            finish();
        }
        utils.showCenterToast(resultArray[1]);
    }

    @Override
    public void onAddressComplete(String address) {
        // TODO Auto-generated method stub
        if (address != null) {
            mLocationAddress = address;
            LatitudeLongitude = tracker.getLatitude() + ", "
                    + tracker.getLongitude();
            locationAddress
                    .setText(LatitudeLongitude + "\n" + mLocationAddress);
        }
    }

    private boolean validate() {
        boolean isValid = true;



        if (reasonSpinner.getSelectedItem().toString() == null
                || reasonSpinner.getSelectedItem().toString()
                .equals("Select")) {
            utils.showCenterToast("Please select fe_reason ");
            isValid = false;
        } else
            UnTracedReason = reasonSpinner.getSelectedItem().toString();


        if (tc_remarksEditText.getText().toString() == null
                || tc_remarksEditText.getText().toString().length() <= 0
                || tc_remarksEditText.getText().toString() == "") {
            utils.showCenterToast("Please enter Fe remarks ");
            isValid = false;
        } else
            feRemark_str = tc_remarksEditText.getText().toString();


        return isValid;
    }

}
