package com.offshoot.avcv.activity;

import java.util.Hashtable;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.offshoot.avcv.R;
import com.offshoot.avcv.db.BackupItem;
import com.offshoot.avcv.db.DbHandler;
import com.offshoot.avcv.bean.CaseItem;
import com.offshoot.avcv.mconfig.MConfig;
import com.offshoot.avcv.parser.MParser;

public class CaseDetailsActivity extends Activity implements
        NetworkTaskCompleteListener {

    ImageView navbarBackIcon;
    LinearLayout navbarBack;
    TextView title;

    void definedAndSetUI() {
        navbarBackIcon = (ImageView) findViewById(R.id.navbarBackIcon);
        navbarBack = (LinearLayout) findViewById(R.id.navbarBack);
        title = (TextView) findViewById(R.id.navbarTitle);
        title.setText("Case Details");
        navbarBackIcon.setVisibility(View.VISIBLE);
        navbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

    Context mContext = CaseDetailsActivity.this;
    CommonUtils utils = new CommonUtils(mContext);
    DbHandler db = new DbHandler(mContext);
    String AVCVDetailsID,dataTypeStr,cycleStr;
    int submitStatus = 0;
    Boolean isResident = false;

    TextView detailsText;
    Button tracedButton, unTraceableButton, wrongAllocatedButton;

    private void showReasonDialog(String AVCVDetailsID) {
        final CharSequence[] choice = {"Address Wrong", "Address Incomplete",
                "Mobile Number wrong", "Mobile Number Not Reachable",
                "Mobile Number Switch off",
                "Invalid Number or Number does not exist"};
        final Boolean[] mSelectedItems = new Boolean[]{false, false, false,
                false, false, false};
        new AlertDialog.Builder(mContext)
                .setMultiChoiceItems(choice, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which, boolean isChecked) {
                                if (isChecked) {
                                    mSelectedItems[which] = true;
                                } else {
                                    mSelectedItems[which] = false;
                                }
                            }
                        })
                .setPositiveButton("Next >>",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                String reason = "";
                                for (int index = 0; index < mSelectedItems.length; index++) {
                                    if (mSelectedItems[index]) {
                                        if (!reason.equalsIgnoreCase("")) {
                                            reason += ", ";
                                        }
                                        reason += (String) choice[index];
                                    }
                                }
                                if (reason.length() > 0) {
                                    dialog.dismiss();
                                    Intent mIntent = new Intent(mContext,
                                            UntraceableActivity.class);
                                    mIntent.putExtra("UnTracedReason", reason);
                                    mIntent.putExtra("AVCVDetailsID",
                                            CaseDetailsActivity.this.AVCVDetailsID);
                                    startActivity(mIntent);
                                    finish();
                                } else {
                                    utils.showCenterToast("Please select reason from options :)");
                                }
                            }
                        }).show();

    }

    void definedVariables() {
        detailsText = (TextView) findViewById(R.id.caseDetailsText);
        tracedButton = (Button) findViewById(R.id.tracedButton);
        unTraceableButton = (Button) findViewById(R.id.unTraceableButton);
        wrongAllocatedButton = (Button) findViewById(R.id.wrongAllocatedButton);
    }

    String mCustomerName = "";

    void readAndShowDetails(String detailsId) {
        CaseItem mItem = db.getCasesFromDetailId(detailsId);

        if (mItem != null) {
            if (mItem.getAddressType().equalsIgnoreCase("RESIDENCE")) {
                isResident = true;
            }


            dataTypeStr=   mItem.getDataType();
          cycleStr=  mItem.getCycleName();

            mCustomerName = mItem.getCustomerName();
            String mDetails = "Mobile Number :-\n" + mItem.getMobileNo()
                    + "\n\nCustomer Name:-\n" + mItem.getCustomerName()
                    + "\n\nAlternate Number:-\n" + mItem.getAlternateNo()
                    + "\n\nAddress:-\n" + mItem.getAVAddress()
                    + "\n\nLankmark:-\n" + mItem.getAVLandmark()
                    + "\n\nCity:-\n" + mItem.getAVCity() + "\n\nDistrict:-\n"
                    + mItem.getAVDistrict() + "\n\nPin Code:-\n"
                    + mItem.getAVPINCode() + "\n\nProduct Type:-\n"
                    + mItem.getAVCVType();

            detailsText.setText(mDetails);
        }
        // Event Listeners
        tracedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if(dataTypeStr.equalsIgnoreCase("C")){

                    Intent mIntent = null;

                    mIntent = new Intent(mContext, CollectionActivity.class);

                    mIntent.putExtra("AVCVDetailsID", AVCVDetailsID);
                    mIntent.putExtra("CYCLE", cycleStr);
                    mIntent.putExtra("DATATYPE", dataTypeStr);

                    startActivity(mIntent);
                    finish();
                }

                if(dataTypeStr.equalsIgnoreCase("V")) {
                    Intent mIntent = null;
                    if (isResident) {
                        mIntent = new Intent(mContext, ResidentFromActivity.class);
                    } else {
                        mIntent = new Intent(mContext, OfficalFromActivityNew.class);
                    }
                    mIntent.putExtra("VerificationID", AVCVDetailsID);
                    mIntent.putExtra("mCustomerName", mCustomerName);
                    startActivity(mIntent);
                    finish();
                }
            }
        });
        unTraceableButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(dataTypeStr.equalsIgnoreCase("V")){
                Intent mIntent = new Intent(mContext,
                        UntraceableActivity.class);
               // mIntent.putExtra("UnTracedReason", reason);
                mIntent.putExtra("VerificationID",
                      AVCVDetailsID);
                startActivity(mIntent);
                finish();}
                if (dataTypeStr.equalsIgnoreCase("C")){
                    Intent mIntent = null;

                    mIntent = new Intent(mContext, CollectionUntraceableActivity.class);

                    mIntent.putExtra("VerificationID", AVCVDetailsID);
                    startActivity(mIntent);
                    finish();
                }
               // showReasonDialog(AVCVDetailsID);
            }
        });
        wrongAllocatedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String[] keys=null;
                String[] enteredValues=null;
                Hashtable<String, String> ht = new Hashtable<String, String>();
                if(dataTypeStr.equalsIgnoreCase("C")){
                    keys = Urls.collectionWrongAllocationkeys;
                    enteredValues = new String[]{AVCVDetailsID,};
                }
                if(dataTypeStr.equalsIgnoreCase("V")){
                    keys = Urls.wrongAllocationkeys;
                    enteredValues = new String[]{utils.getStringData("LoginId"), AVCVDetailsID,};
                }

                for (int i = 0; i < keys.length; i++) {
                    ht.put(keys[i], enteredValues[i]);
                }
                if (utils.isNetworkAvailable(mContext)) {
                    if(dataTypeStr.equalsIgnoreCase("V")) {
                        new NetworkTask(mContext, ht, true)
                                .execute(Urls.submitVisitStatusUrl);
                    }
                    if(dataTypeStr.equalsIgnoreCase("C")){
                        new NetworkTask(mContext, ht, true)
                                .execute(Urls.submitCollectionWAUrl);
                    }

                } else {
                    BackupItem mItem = new BackupItem();
                    mItem.setUserId(utils.getStringData("LoginId"));
                    mItem.setAvcvId(AVCVDetailsID);
                    mItem.setAvcvData(utils.convertArrayToString(enteredValues));
                    mItem.setType("wrong");
                    db.insertIntoBackUp(mItem);
                    db.deleteFromCases(AVCVDetailsID);
                    finish();

                    utils.showCenterToast(MConfig.internetMessage
                            + "\nYour data has been saved in local storage!!");
                }
                ht = null;
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_case_details);
        definedAndSetUI();
        definedVariables();
        AVCVDetailsID = this.getIntent().getExtras().getString("VerificationID");
        readAndShowDetails(AVCVDetailsID);

    }

    @Override
    public void onNetworkComplete(String result, String type) {
        // TODO Auto-generated method stub
        String[] resultArray = new MParser(mContext).parseXMLResponse(result);
        if (Boolean.parseBoolean(resultArray[0])) {
            // TODO
            utils.showCenterToast("Wrong Allocation Result\n" + resultArray[1]);
            db.deleteFromCases(AVCVDetailsID);
            finish();
        } else {
            utils.showCenterToast("Wrong Allocation Result\n" + resultArray[1]);
        }
    }

}
