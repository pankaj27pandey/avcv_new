package com.offshoot.avcv.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.common.Validation;
import com.apzone.android.common.XUI;
import com.apzone.android.crop.Crop;
import com.apzone.android.location.AddressTask;
import com.apzone.android.location.AddressTaskCompleteListener;
import com.apzone.android.location.GPSTracker;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.offshoot.avcv.R;
import com.offshoot.avcv.bean.DispositionBean;
import com.offshoot.avcv.db.BackupItem;
import com.offshoot.avcv.db.DbHandler;
import com.offshoot.avcv.listadapters.CustomAdapter;
import com.offshoot.avcv.mconfig.MConfig;
import com.offshoot.avcv.parser.MParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

public class CollectionUntraceableActivity extends Activity implements
        AddressTaskCompleteListener, NetworkTaskCompleteListener {
    ImageView navbarBackIcon;
    LinearLayout navbarBack;
    TextView title;
    Context mContext = CollectionUntraceableActivity.this;
    CommonUtils utils = new CommonUtils(mContext);
    XUI xui = new XUI(mContext);
    DbHandler db = new DbHandler(mContext);
    GPSTracker tracker = GPSTracker.getInstance();
    Calendar c = Calendar.getInstance();
    Validation valid;
    TextView locationAddress, tv_reason2;
    ImageView customerImage, houseImage, image3;
    Button submitButton;
    String dataType, AVCVDetailsID, UnTracedReason, feRemark_str, mLocationAddress, customerImageBytes,
            houseImageBytes, image3Bytes, LatitudeLongitude, feStatus, feReason1, feReason2 = "", type;
    byte[] userImageBlobData = null;
    byte[] placeImageBlobData = null;
    byte[] image3BlobData = null;
    int imageType = 1;
    EditText tc_remarksEditText;
    Spinner reasonSpinner, reason2Spinner;
    List<String> actualReason2List = new ArrayList<>();
    ArrayAdapter<String> adapter;
    List<DispositionBean> feReasonBeansList = new ArrayList<>();
    CustomAdapter customAdapter;
    boolean checkGPSStatus = true;

    void definedAndSetUI(String dataType) {
        navbarBackIcon = findViewById(R.id.navbarBackIcon);
        navbarBack = findViewById(R.id.navbarBack);
        title = findViewById(R.id.navbarTitle);

        title.setText("Untraceable Form");
        feStatus = "UNTRACEABLE";


        navbarBackIcon.setVisibility(View.VISIBLE);
        navbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

    String[] checkEnteredData() {

        String[] mValues = {AVCVDetailsID, feStatus, UnTracedReason, feReason2, feRemark_str, LatitudeLongitude,
                mLocationAddress, customerImageBytes, houseImageBytes, image3Bytes};
        return mValues;

    }

    void defiendVaribles() {
        locationAddress = findViewById(R.id.locationAddress);
        reasonSpinner = findViewById(R.id.reasonSpinner);
        reason2Spinner = findViewById(R.id.reason2Spinner);
        tv_reason2 = findViewById(R.id.tv_reason2);
        tc_remarksEditText = findViewById(R.id.tc_remarksEditText);
        customerImage = findViewById(R.id.customerImage);
        houseImage = findViewById(R.id.houseImage);
        image3 = findViewById(R.id.image3);
        submitButton = findViewById(R.id.submitButton);

        type = "UNTRACEABLE";
        feReasonBeansList = db.getFeReason(type, "COL");

        // Resources passed to adapter to get image
        Resources res = getResources();
        // Create custom adapter object ( see below CustomAdapter.java )
        customAdapter = new CustomAdapter(mContext, R.layout.spinner_row, (ArrayList) feReasonBeansList, res);
        customAdapter.setDropDownViewResource(R.layout.spinner_row);
        reasonSpinner.setAdapter(customAdapter);


    }

    void addEventListeners() {
        reasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                DispositionBean user = (DispositionBean) adapterView.getSelectedItem();


                if (user.getFE_REASON2().isEmpty()) {
                    reason2Spinner.setVisibility(View.GONE);
                    tv_reason2.setVisibility(View.GONE);
                } else {
                    reason2Spinner.setVisibility(View.VISIBLE);
                    tv_reason2.setVisibility(View.VISIBLE);
                    actualReason2List = new ArrayList<>(Arrays.asList(user.getFE_REASON2().split("/")));
                    actualReason2List.add(0, "Select");
                    adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, actualReason2List);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    reason2Spinner.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        customerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                imageType = 1;
                xui.createImagePickOptionDialog();
            }
        });
        houseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                imageType = 2;
                xui.createImagePickOptionDialog();
            }
        });
        image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                imageType = 3;
                xui.createImagePickOptionDialog();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    String[] enteredValues = checkEnteredData();
                    if (enteredValues != null) {
                        Hashtable<String, String> ht = new Hashtable<String, String>();
                        String[] keys = Urls.untraceableCollectionFormkeys;
                        for (int i = 0; i < keys.length; i++) {
                            ht.put(keys[i], enteredValues[i] + "");
                        }
                        if (utils.isNetworkAvailable(mContext)) {
                            new NetworkTask(mContext, ht, true)
                                    .execute(Urls.submitCollectionUntraceableUrl);
                        } else {
                            try {

                                int mLength = enteredValues.length;
                                enteredValues[mLength - 2] = "SavedUserImage";
                                enteredValues[mLength - 1] = "SavedPlaceImage";
                                BackupItem mItem = new BackupItem();
                                mItem.setUserId(utils.getStringData("LoginId"));
                                mItem.setAvcvId(AVCVDetailsID);
                                mItem.setAvcvData(utils
                                        .convertArrayToString(enteredValues));
                                mItem.setType("untrace");
                                mItem.setUserImage(userImageBlobData);
                                mItem.setPlaceImage(placeImageBlobData);
                                db.insertIntoBackUp(mItem);
                                db.deleteFromCases(AVCVDetailsID);
                                finish();
                                utils.showCenterToast(MConfig.internetMessage
                                        + "\nYour data has been saved in local storage!!");
                            } catch (Exception ee) {
                                utils.showCenterToast("Unable to save data"
                                        + "\n" + ee.getMessage());
                            }
                        }
                        ht = null;
                    }
                }

                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.activity_collection_untracable);
        Bundle mBundel = getIntent().getExtras();
        AVCVDetailsID = mBundel.getString("VerificationID");
        dataType = this.getIntent().getExtras().getString("DATATYPE");

        //  UnTracedReason = mBundel.getString("UnTracedReason");
        definedAndSetUI(dataType);
        defiendVaribles();
        addEventListeners();
        valid = new Validation(this);

    }

    void checkForGPS() {
        tracker.getLocation(this);
        if (tracker.getLatitude() == 0.0) {
            if (utils.isNetworkAvailable(mContext)) {
                tracker.showSettingsAlert("Error",
                        "Please check your GPS and Internet Settings :)");
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get longitude and latitude due to internet connection !!");
            }
        } else {
            if (utils.isNetworkAvailable(mContext)) {
                new AddressTask(this).execute(tracker.getLatitude(),
                        tracker.getLongitude());
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get location address due to internet connection !!");
            }
        }
        checkGPSStatus = false;
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        // you can use this in "if" to test if location is updated ok
        checkForGPS();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent result) {
        byte[] imageArray = Crop.getImageBitmap(requestCode, resultCode,
                result, (Activity) mContext, false);
        if (imageArray != null) {
            ImageView resultImageView = null;
            String base64StringImage = Base64.encodeToString(imageArray, 0);
            switch (imageType) {
                case 1:
                    userImageBlobData = imageArray;
                    resultImageView = customerImage;
                    customerImageBytes = base64StringImage;
                    break;
                case 2:
                    placeImageBlobData = imageArray;
                    resultImageView = houseImage;
                    houseImageBytes = base64StringImage;
                    break;
                case 3:
                    image3BlobData = imageArray;
                    resultImageView = image3;
                    image3Bytes = base64StringImage;
                    break;
            }
            resultImageView.setImageDrawable(null);
            Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0,
                    imageArray.length);
            Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system

            Canvas cs = new Canvas(mutableBitmap);
            Paint tPaint = new Paint();
            tPaint.setTextSize(35);
            tPaint.setColor(Color.RED);
            tPaint.setStyle(Paint.Style.FILL);
            cs.drawBitmap(mutableBitmap, 0f, 0f, null);
            float height = tPaint.measureText("yY");
            cs.drawText(dateTime, 20f, height + 15f, tPaint);
            resultImageView.setImageBitmap(mutableBitmap);
        }
    }

    @Override
    public void onNetworkComplete(String result, String type) {
        // TODO Auto-generated method stub
        String[] resultArray = new MParser(mContext).parseXMLResponse(result);

        if (Boolean.parseBoolean(resultArray[0])) {
            db.deleteFromCases(AVCVDetailsID);
           /* Intent mIntent = new Intent(mContext, MainActivity.class);
            startActivity(mIntent);
            finishAffinity();*/
            finish();
           /* startActivity(new Intent(mContext, DownloadedCasesActivity.class));
            finish();*/
        }
        utils.showCenterToast(resultArray[1]);
    }

    @Override
    public void onAddressComplete(String address) {
        // TODO Auto-generated method stub
        if (address != null) {
            mLocationAddress = address;
            LatitudeLongitude = tracker.getLatitude() + ", "
                    + tracker.getLongitude();
            locationAddress
                    .setText(LatitudeLongitude + "\n" + mLocationAddress);
        }
    }

    private boolean validate() {
        boolean isValid = true;


        if (reasonSpinner.getSelectedItem().toString() == null
                || reasonSpinner.getSelectedItem().toString()
                .equals("Select")) {
            utils.showCenterToast("Please select fe_reason ");
            isValid = false;
        } else {
            DispositionBean feReasonBean = (DispositionBean) reasonSpinner.getSelectedItem();
            UnTracedReason = feReasonBean.getFE_REASON1();
        }
        // UnTracedReason = reasonSpinner.getSelectedItem().toString();


        if (reason2Spinner.isShown()) {
            if (reason2Spinner.getSelectedItem().toString() == null
                    || reason2Spinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select fe_Status ");
                isValid = false;
            } else
                feReason2 = reason2Spinner.getSelectedItem().toString();
        } else {

            feReason2 = "";
        }


        if (tc_remarksEditText.getText().toString() == null
                || tc_remarksEditText.getText().toString().length() <= 0
                || tc_remarksEditText.getText().toString() == "") {
            utils.showCenterToast("Please enter Fe remarks ");
            isValid = false;
        } else
            feRemark_str = tc_remarksEditText.getText().toString();


        return isValid;
    }

}
