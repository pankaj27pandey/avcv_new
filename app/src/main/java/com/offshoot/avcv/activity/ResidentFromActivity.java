package com.offshoot.avcv.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.common.Validation;
import com.apzone.android.common.XUI;
import com.apzone.android.crop.Crop;
import com.apzone.android.location.AddressTask;
import com.apzone.android.location.AddressTaskCompleteListener;
import com.apzone.android.location.GPSTracker;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.offshoot.avcv.R;
import com.offshoot.avcv.db.BackupItem;
import com.offshoot.avcv.db.DbHandler;
import com.offshoot.avcv.mconfig.MConfig;
import com.offshoot.avcv.parser.MParser;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Hashtable;

import static com.apzone.android.common.AppConstants.SIGNATURE_ACTIVITY;

public class ResidentFromActivity extends Activity implements
        AddressTaskCompleteListener, NetworkTaskCompleteListener {
    ImageView navbarBackIcon;
    LinearLayout navbarBack;
    TextView title,tvAvcvReason;
    String accessibilityStr, typeofLocalityStr, standardLivingStr, houseTypeStr, relationShipStr,
            ownerShipStr,
            accomodationStr, maritalStr, purchagePurposeStr, productDeliveredStr, n1StatusStr,
            n2StatusStr, avcvStatusStr, negativeReasonStr, personMeetStr, stayingSinceStr, rentPaidStr, numFamilyMembersStr,

    earningMembersStr, workingSinceStr, natureOfBusinessStr, designationStr,
            addressStr, netSalaryProfitStr, n1NameStr,
            n2NameStr, roadmapLandmarkStr, cpvRemarksStr, typeOfEmploymentStr,meterStatusStr;

    Context mContext = ResidentFromActivity.this;
    CommonUtils utils = new CommonUtils(mContext);
    XUI xui = new XUI(mContext);
    DbHandler db = new DbHandler(mContext);
    GPSTracker tracker = GPSTracker.getInstance();
    Calendar c = Calendar.getInstance();
    Validation valid;
    String[] enteredValues;

    Spinner officeAccessibilitySpinner,
            standardLivingSpinner, houseTypeSpinner,
            relationSpinner, ownershipSpinner, accomodationSpinner,
            maritalStatusSpinner, purchasePurposeSpinner,
            productDeliveredSpinner, n1StatusSpinner,
            n2StatusSpinner,meterStatusSpinner, avcvStatusSpinner, negativeReasonSpinner, typeOfEmploymentSpinner;

    EditText personMeetEditText, stayingSince, rentPaid, numFamilyMembers,
            earningMembers,
            workingSince, natureOfBusiness, designation, address,
            netSalaryProfit, n1Name,
            n2Name, roadmapLandmark,
            cpvRemarks;

    TextView locationAddress, verifierName;
    ImageView customerSignImage, customerImage, houseImage, ivImage_3,meterImage,ivImage_4,ivImage_5;
    Button submitButton;
    ScrollView residentActivityScrollView;

    String AVCVDetailsID;
    String mLocationAddress;
    int imageType = 0;
    int sinceDatePickerType = 0;
    String signImageBytes = null;
    String customerImageBytes = null;
    String houseImageBytes = null;
    String ivImage_3Bytes = null;
    String meterImageBytes = null;
    String Image4ImageBytes = null;
    String Image5ImageBytes = null;
    byte[] signImageBlobData = null;
    byte[] userImageBlobData = null;
    byte[] placeImageBlobData = null;
    byte[] ivImage_3BlobData = null;
    byte[] meterImageBlobData = null;
    byte[] image4BlobData = null;
    byte[] image5BlobData = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recident_form);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        AVCVDetailsID = getIntent().getExtras().getString("VerificationID");
        definedAndSetUI();
        defiendVaribles();
        addEventListeners();
        valid = new Validation(this, residentActivityScrollView);
        verifierName.setText(utils.getStringData("LoginName"));
    }


    void definedAndSetUI() {
        navbarBackIcon = (ImageView) findViewById(R.id.navbarBackIcon);
        navbarBack = (LinearLayout) findViewById(R.id.navbarBack);
        title = (TextView) findViewById(R.id.navbarTitle);
        title.setText("Resident Form");
        navbarBackIcon.setVisibility(View.VISIBLE);
        navbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }


    String[] checkEnteredData() {

        if (stayingSince.getText().toString().equals("Select")) {
            utils.showCenterToast("Please Select Staying Since Date !!");
            return null;
        }

     /*   if (!valid.isValidImage(signImageBytes, "Customer Sign Image")
                || !valid.isValidImage(customerImageBytes, " Image 1")
                || !valid.isValidImage(houseImageBytes, " Image 2")) {
            return null;
        }*/

        String AVCVReason = "";
//        if (!negativeReasonSpinner.getSelectedItem().toString()
//                .equals("Select")) {
//            AVCVReason = negativeReasonSpinner.getSelectedItem().toString();
//        }

        String[] mValues = {utils.getStringData("LoginId"),AVCVDetailsID,
                accessibilityStr, houseTypeStr, standardLivingStr, personMeetStr,
                relationShipStr, ownerShipStr, rentPaidStr, accomodationStr,
                stayingSince.getText().toString(), maritalStr, numFamilyMembersStr,
                earningMembersStr, purchagePurposeStr, productDeliveredStr,
                typeOfEmploymentStr, natureOfBusinessStr, designationStr,
                workingSince.getText().toString(), netSalaryProfitStr, n1NameStr,
                n1StatusStr, n2NameStr, n2StatusStr, verifierName.getText().toString(),
                roadmapLandmark.getText().toString(),meterStatusStr, avcvStatusStr, negativeReasonStr, cpvRemarksStr,
                tracker.getLatitude() + ", " + tracker.getLongitude(), mLocationAddress,
                signImageBytes,meterImageBytes, customerImageBytes, houseImageBytes, ivImage_3Bytes,Image4ImageBytes,Image5ImageBytes
               /* address.getText().toString(),*/
        };

        return mValues;
    }

    void defiendVaribles() {
        officeAccessibilitySpinner = (Spinner) findViewById(R.id.officeAccessibilitySpinner);
        standardLivingSpinner = (Spinner) findViewById(R.id.standardLivingSpinner);
        houseTypeSpinner = (Spinner) findViewById(R.id.houseTypeSpinner);
        relationSpinner = (Spinner) findViewById(R.id.relationSpinner);
        ownershipSpinner = (Spinner) findViewById(R.id.ownershipSpinner);
        accomodationSpinner = (Spinner) findViewById(R.id.accomodationSpinner);
        maritalStatusSpinner = (Spinner) findViewById(R.id.maritalStatusSpinner);
        purchasePurposeSpinner = (Spinner) findViewById(R.id.purchasePurposeSpinner);
        productDeliveredSpinner = (Spinner) findViewById(R.id.productDeliveredSpinner);
        n1StatusSpinner = (Spinner) findViewById(R.id.n1StatusSpinner);
        n2StatusSpinner = (Spinner) findViewById(R.id.n2StatusSpinner);
        avcvStatusSpinner = (Spinner) findViewById(R.id.avcvStatusSpinner);
        meterStatusSpinner= (Spinner) findViewById(R.id.meterStatusSpinner);
        negativeReasonSpinner = (Spinner) findViewById(R.id.negativeReasonSpinner);
        typeOfEmploymentSpinner = (Spinner) findViewById(R.id.typeOfEmploymentSpinner);

        tvAvcvReason=(TextView)findViewById(R.id.tvAvcvReason);

        personMeetEditText = (EditText) findViewById(R.id.personMeetEditText);
        stayingSince = (EditText) findViewById(R.id.stayingSince);
        rentPaid = (EditText) findViewById(R.id.rentPaid);
        numFamilyMembers = (EditText) findViewById(R.id.numFamilyMembers);
        earningMembers = (EditText) findViewById(R.id.earningMembers);

        workingSince = (EditText) findViewById(R.id.workingSince);
        natureOfBusiness = (EditText) findViewById(R.id.natureOfBusiness);
        designation = (EditText) findViewById(R.id.designation);
        address = (EditText) findViewById(R.id.address);
        netSalaryProfit = (EditText) findViewById(R.id.netSalaryProfit);

        n1Name = (EditText) findViewById(R.id.n1Name);
        n2Name = (EditText) findViewById(R.id.n2Name);
        roadmapLandmark = (EditText) findViewById(R.id.roadmapLandmark);
        cpvRemarks = (EditText) findViewById(R.id.cpvRemarks);

        customerSignImage = (ImageView) findViewById(R.id.customerSignImage);
        customerImage = (ImageView) findViewById(R.id.customerImage);
        houseImage = (ImageView) findViewById(R.id.houseImage);
        ivImage_3 = (ImageView) findViewById(R.id.image_3);
        ivImage_4 = (ImageView) findViewById(R.id.image_4);
        ivImage_5 = (ImageView) findViewById(R.id.image_5);
        meterImage = (ImageView) findViewById(R.id.meterImage);

        verifierName = (TextView) findViewById(R.id.verifierName);
        locationAddress = (TextView) findViewById(R.id.locationAddress);

        submitButton = (Button) findViewById(R.id.submitButton);
        residentActivityScrollView = (ScrollView) findViewById(R.id.residentActivityScrollView);
    }

    void addEventListeners() {


        customerSignImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//				imageType = 1;
//				Crop.pickImage((Activity) mContext, 2);
                // TODO Auto-generated method stub
                Intent intent = new Intent(mContext, CaptureSignatureActivity.class);
                startActivityForResult(intent, SIGNATURE_ACTIVITY);
            }
        });

        meterImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 2;
                xui.createImagePickOptionDialog();
            }
        });
        customerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imageType = 3;
                xui.createImagePickOptionDialog();
            }
        });
        houseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 4;
                xui.createImagePickOptionDialog();
            }
        });
        ivImage_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 5;
                xui.createImagePickOptionDialog();
            }
        });
        ivImage_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 6;
                xui.createImagePickOptionDialog();
            }
        });
        ivImage_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 7;
                xui.createImagePickOptionDialog();
            }
        });
        stayingSince.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                sinceDatePickerType = 0;
                showDialog(0);
            }
        });
        workingSince.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                sinceDatePickerType = 1;
                showDialog(0);
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (validate()) {
                    enteredValues = checkEnteredData();
                    if (enteredValues != null) {
                        if (utils.isNetworkAvailable(mContext)) {
                            Hashtable<String, String> ht = new Hashtable<String, String>();
                            String[] keys = Urls.ResidentFormkeys;
                            for (int i = 0; i < keys.length; i++) {
                                ht.put(keys[i], enteredValues[i] + "");
                            }
                            new NetworkTask(mContext, ht, true)
                                    .execute(Urls.submitResidentForm);
                            ht = null;
                        } else {
                            try {
                                int mLength = enteredValues.length;
                                enteredValues[mLength - 3] = "SavedSignImage";
                                enteredValues[mLength - 2] = "SavedUserImage";
                                enteredValues[mLength - 1] = "SavedPlaceImage";
                                BackupItem mItem = new BackupItem();
                                mItem.setUserId(utils.getStringData("LoginId"));
                                mItem.setAvcvId(AVCVDetailsID);
                                mItem.setAvcvData(utils
                                        .convertArrayToString(enteredValues));
                                mItem.setType("resident");
                                mItem.setSignImage(signImageBlobData);
                                mItem.setUserImage(userImageBlobData);
                                mItem.setPlaceImage(placeImageBlobData);
                                db.insertIntoBackUp(mItem);
                                db.deleteFromCases(AVCVDetailsID);
                                finish();

                                utils.showCenterToast(MConfig.internetMessage
                                        + "\nYour data has been saved in local storage!!");
                            } catch (Exception ee) {
                                utils.showCenterToast("Unable to save data" + "\n"
                                        + ee.getMessage());
                            }
                        }
                    } else {
                        utils.showCenterToast("Please enter all the required values!!");
                    }
                }
            }
        });

        typeOfEmploymentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View view, int position, long id) {
                String selectedItem = parent
                        .getItemAtPosition(position).toString();
                if (selectedItem.equals("Own Business")) {
                    natureOfBusiness.setVisibility(View.VISIBLE);
                    designation.setVisibility(View.GONE);
                } else if (selectedItem.equalsIgnoreCase("Govt. Job")||selectedItem.equalsIgnoreCase("Pvt Job")) {
                    designation.setVisibility(View.VISIBLE);
                    natureOfBusiness.setVisibility(View.GONE);
                } else {
                    designation.setVisibility(View.GONE);
                    natureOfBusiness.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        avcvStatusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent,
                                       View view, int position, long id) {
                String selectedItem = parent
                        .getItemAtPosition(position).toString();
                if (selectedItem.equals("Negative")) {
                    tvAvcvReason.setVisibility(View.VISIBLE);
                    negativeReasonSpinner.setVisibility(View.VISIBLE);

                } else {
                    negativeReasonSpinner.setVisibility(View.GONE);
                    tvAvcvReason.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    boolean checkGPSStatus = true;

    void checkForGPS() {
        tracker.getLocation(this);
        if (tracker.getLatitude() == 0.0) {
            if (utils.isNetworkAvailable(mContext)) {
                tracker.showSettingsAlert("Error",
                        "Please check your GPS and Internet Settings :)");
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get longitude and latitude due to internet connection !!");
            }
        } else {
            if (utils.isNetworkAvailable(mContext)) {
                new AddressTask(this).execute(tracker.getLatitude(),
                        tracker.getLongitude());
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get location address due to internet connection !!");
            }
        }
        checkGPSStatus = false;
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        // you can use this in "if" to test if location is updated ok
        checkForGPS();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                DatePickerDialog dpd = new DatePickerDialog(mContext,
                        pickerListener, c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dpd.setTitle("Month and Year");
                try {

                    Field[] datePickerDialogFields = dpd.getClass()
                            .getDeclaredFields();
                    for (Field datePickerDialogField : datePickerDialogFields) {
                        if (datePickerDialogField.getName().equals("mDatePicker")) {
                            datePickerDialogField.setAccessible(true);
                            DatePicker datePicker = (DatePicker) datePickerDialogField
                                    .get(dpd);
                            Field datePickerFields[] = datePickerDialogField
                                    .getType().getDeclaredFields();
                            for (Field datePickerField : datePickerFields) {
                                if ("mDayPicker".equals(datePickerField.getName())
                                        || "mDaySpinner".equals(datePickerField
                                        .getName())) {
                                    datePickerField.setAccessible(true);
                                    Object dayPicker = new Object();
                                    dayPicker = datePickerField.get(datePicker);
                                    ((View) dayPicker).setVisibility(View.GONE);
                                }
                            }
                        }

                    }
                } catch (Exception ex) {
                }
                return dpd;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub

            // Show selected date
            StringBuilder selectedDate = new StringBuilder()
                    .append(monthOfYear + 1).append("-").append(year)
                    .append(" ");
            if (sinceDatePickerType == 0) {
                stayingSince.setText(selectedDate);
            } else {
                workingSince.setText(selectedDate);
            }
        }

    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent result) {
        ImageView resultImageView = null;
        byte[] imageArray = null;
        if (resultCode == RESULT_OK)
            if (requestCode == SIGNATURE_ACTIVITY) {
                imageArray = result.getByteArrayExtra("byteArray");
                String base64StringImage = Base64.encodeToString(imageArray, 0);
                signImageBlobData = imageArray;
                resultImageView = customerSignImage;
                signImageBytes = base64StringImage;
            } else {
                imageArray = Crop.getImageBitmap(requestCode, resultCode,
                        result, (Activity) mContext, false);
                if (imageArray != null) {
                    String base64StringImage = Base64.encodeToString(
                            imageArray, 0);
                    switch (imageType) {
                        case 2:
                            meterImageBlobData = imageArray;
                            resultImageView = meterImage;
                            meterImageBytes = base64StringImage;
                            break;
                        case 3:
                            userImageBlobData = imageArray;
                            resultImageView = customerImage;
                            customerImageBytes = base64StringImage;

                            break;
                        case 4:
                            placeImageBlobData = imageArray;
                            resultImageView = houseImage;
                            houseImageBytes = base64StringImage;

                            break;
                        case 5:
                            ivImage_3BlobData = imageArray;
                            resultImageView = ivImage_3;
                            ivImage_3Bytes = base64StringImage;

                            break;
                        case 6:

                            image4BlobData = imageArray;
                            resultImageView = ivImage_4;
                            Image4ImageBytes = base64StringImage;
                            break;
                        case 7:
                            image5BlobData = imageArray;
                            resultImageView = ivImage_5;
                            Image5ImageBytes = base64StringImage;
                            break;
                    }

                }

            }
        if (resultImageView != null) {

            resultImageView.setImageDrawable(null);
            Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0,
                    imageArray.length);
            resultImageView.setImageBitmap(bmp);
//			resultImafgeView.setImageDrawable(null);
//			Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0,
//					imageArray.length);
//			Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
//
//
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system
//
//			Canvas cs = new Canvas(mutableBitmap);
//			Paint tPaint = new Paint();
//			tPaint.setTextSize(35);
//			tPaint.setColor(Color.RED);
//			tPaint.setStyle(Paint.Style.FILL);
//			cs.drawBitmap(mutableBitmap, 0f, 0f, null);
//			float height = tPaint.measureText("yY");
//			cs.drawText(dateTime, 20f, height + 15f, tPaint);
//			resultImageView.setImageBitmap(mutableBitmap);
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        xui = null;
        utils = null;
        valid = null;
        db = null;
        if (tracker != null) {
            tracker.stopUsingGPS();
        }
    }

    @Override
    public void onNetworkComplete(String result, String type) {
        // TODO Auto-generated method stub
        String[] resultArray = new MParser(mContext).parseXMLResponse(result);
        if (Boolean.parseBoolean(resultArray[0])) {
            db.deleteFromCases(AVCVDetailsID);
            finish();
        }
//        utils.showCenterToast(result);

        utils.showCenterToast(resultArray[1]);
    }

    @Override
    public void onAddressComplete(String address) {
        // TODO Auto-generated method stub
        if (address != null) {
            mLocationAddress = address;
            locationAddress.setText(tracker.getLatitude() + ", "
                    + tracker.getLongitude() + "\n" + mLocationAddress);
        }
    }


    private boolean validate() {
        boolean isValid = true;
        if (officeAccessibilitySpinner.isShown()) {
            if (officeAccessibilitySpinner.getSelectedItem().toString() == null
                    || officeAccessibilitySpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Accessibility ");
                isValid = false;
            } else
                accessibilityStr = officeAccessibilitySpinner.getSelectedItem().toString();
        } else {

            accessibilityStr = "";
        }


        if (standardLivingSpinner.isShown()) {
            if (standardLivingSpinner.getSelectedItem().toString() == null
                    || standardLivingSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Standard Living ");
                isValid = false;
            } else
                standardLivingStr = standardLivingSpinner.getSelectedItem().toString();
        } else {

            standardLivingStr = "";
        }
        if (houseTypeSpinner.isShown()) {
            if (houseTypeSpinner.getSelectedItem().toString() == null
                    || houseTypeSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select House Type ");
                isValid = false;
            } else
                houseTypeStr = houseTypeSpinner.getSelectedItem().toString();
        } else {

            houseTypeStr = "";
        }
        if (relationSpinner.isShown()) {
            if (relationSpinner.getSelectedItem().toString() == null
                    || relationSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select RelationShip ");
                isValid = false;
            } else
                relationShipStr = relationSpinner.getSelectedItem().toString();
        } else {

            relationShipStr = "";
        }
        if (ownershipSpinner.isShown()) {
            if (ownershipSpinner.getSelectedItem().toString() == null
                    || ownershipSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select OwnerShip ");
                isValid = false;
            } else
                ownerShipStr = ownershipSpinner.getSelectedItem().toString();
        } else {

            ownerShipStr = "";
        }
        if (accomodationSpinner.isShown()) {
            if (accomodationSpinner.getSelectedItem().toString() == null
                    || accomodationSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Accomodation ");
                isValid = false;
            } else
                accomodationStr = accomodationSpinner.getSelectedItem().toString();
        } else {

            accomodationStr = "";
        }//
        if (maritalStatusSpinner.isShown()) {
            if (maritalStatusSpinner.getSelectedItem().toString() == null
                    || maritalStatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Marital Status ");
                isValid = false;
            } else
                maritalStr = maritalStatusSpinner.getSelectedItem().toString();
        } else {

            maritalStr = "";
        }
        if (purchasePurposeSpinner.isShown()) {
            if (purchasePurposeSpinner.getSelectedItem().toString() == null
                    || purchasePurposeSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Purchase Purpose ");
                isValid = false;
            } else
                purchagePurposeStr = purchasePurposeSpinner.getSelectedItem().toString();
        } else {

            purchagePurposeStr = "";
        }
        if (productDeliveredSpinner.isShown()) {
            if (productDeliveredSpinner.getSelectedItem().toString() == null
                    || productDeliveredSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Product Delivered ");
                isValid = false;
            } else
                productDeliveredStr = productDeliveredSpinner.getSelectedItem().toString();
        } else {

            productDeliveredStr = "";
        }

        if (typeOfEmploymentSpinner.isShown()) {
            if (typeOfEmploymentSpinner.getSelectedItem().toString() == null
                    || typeOfEmploymentSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Type Of Employment ");
                isValid = false;
            } else
                typeOfEmploymentStr = typeOfEmploymentSpinner.getSelectedItem().toString();
        } else {

            typeOfEmploymentStr = "";
        }


        if (n1StatusSpinner.isShown()) {
            if (n1StatusSpinner.getSelectedItem().toString() == null
                    || n1StatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select N1 Status ");
                isValid = false;
            } else
                n1StatusStr = n1StatusSpinner.getSelectedItem().toString();
        } else {

            n1StatusStr = "";
        }//
        if (n2StatusSpinner.isShown()) {
            if (n2StatusSpinner.getSelectedItem().toString() == null
                    || n2StatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select N2 Status ");
                isValid = false;
            } else
                n2StatusStr = n2StatusSpinner.getSelectedItem().toString();
        } else {

            n2StatusStr = "";
        }

        if (meterStatusSpinner.isShown()) {
            if (meterStatusSpinner.getSelectedItem().toString() == null
                    || meterStatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Meter Status ");
                isValid = false;
            } else
                meterStatusStr = meterStatusSpinner.getSelectedItem().toString();
        } else {

            meterStatusStr = "";
        }

        if (avcvStatusSpinner.isShown()) {
            if (avcvStatusSpinner.getSelectedItem().toString() == null
                    || avcvStatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Avcv Status ");
                isValid = false;
            } else
                avcvStatusStr = avcvStatusSpinner.getSelectedItem().toString();
        } else {

            avcvStatusStr = "";
        }
        if (negativeReasonSpinner.isShown()) {
            if (negativeReasonSpinner.getSelectedItem().toString() == null
                    || negativeReasonSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Negative Reason  ");
                isValid = false;
            } else
                negativeReasonStr = negativeReasonSpinner.getSelectedItem().toString();
        } else {

            negativeReasonStr = "";
        }
//        if (nextAppointmentTv.isShown()) {
//            if (nextAppointmentTv.getText().toString() == null
//                    || nextAppointmentTv.getText().toString().length() <= 0
//                    || nextAppointmentTv.getText().toString() == "") {
//                utils.showCenterToast("Please enter next appointment date ");
//                isValid = false;
//            } else if (nextAppointmentTimeTv.getText().toString() == null
//                    || nextAppointmentTimeTv.getText().toString() == ""
//                    || nextAppointmentTimeTv.getText().toString().length() <= 0) {
//                utils.showCenterToast("Please enter next appointment time  ");
//                isValid = false;
//            } else {
//                String minuString = "";
//                String hourString = "";
//                if (minute < 10)
//                    minuString = "0" + minute;
//                else
//                    minuString = String.valueOf(minute);
//                if (hour < 10)
//                    hourString = "0" + hour;
//                else
//                    hourString = String.valueOf(hour);
//
//                nextAppString = nextAppointmentTv.getText().toString()
//                        + hourString + minuString;
//            }
//        } else {
//            nextAppString = "";
//        }

        if (personMeetEditText.isShown()) {
            if (personMeetEditText.getText().toString() == null
                    || personMeetEditText.getText().toString().length() <= 0
                    || personMeetEditText.getText().toString() == "") {
                utils.showCenterToast("Please enter Person Meet ");
                isValid = false;
            } else
                personMeetStr = personMeetEditText.getText().toString();
        } else {
            personMeetStr = "";
        }

        if (rentPaid.isShown()) {
            if (rentPaid.getText().toString() == null
                    || rentPaid.getText().toString().length() <= 0
                    || rentPaid.getText().toString() == "") {
                utils.showCenterToast("Please enter Rent Paid ");
                isValid = false;
            } else
                rentPaidStr = rentPaid.getText().toString();
        } else {
            rentPaidStr = "";
        }

        if (numFamilyMembers.isShown()) {
            if (numFamilyMembers.getText().toString() == null
                    || numFamilyMembers.getText().toString().length() <= 0
                    || numFamilyMembers.getText().toString() == "") {
                utils.showCenterToast("Please enter Number of Family Member ");
                isValid = false;
            } else
                numFamilyMembersStr = numFamilyMembers.getText().toString();
        } else {
            numFamilyMembersStr = "";

        }

        if (earningMembers.isShown()) {
            if (earningMembers.getText().toString() == null
                    || earningMembers.getText().toString().length() <= 0
                    || earningMembers.getText().toString() == "") {
                utils.showCenterToast("Please enter No. of Earning Member ");
                isValid = false;
            } else
                earningMembersStr = earningMembers.getText().toString();
        } else {
            earningMembersStr = "";

        }


        if (natureOfBusiness.isShown()) {
            if (natureOfBusiness.getText().toString() == null
                    || natureOfBusiness.getText().toString().length() <= 0
                    || natureOfBusiness.getText().toString() == "") {
                utils.showCenterToast("Please enter Employment Details ");
                isValid = false;
            } else
                natureOfBusinessStr = natureOfBusiness.getText().toString();
        } else {
            natureOfBusinessStr = "";

        }

        if (designation.isShown()) {
            if (designation.getText().toString() == null
                    || designation.getText().toString().length() <= 0
                    || designation.getText().toString() == "") {
                utils.showCenterToast("Please enter Designation ");
                isValid = false;
            } else
                designationStr = designation.getText().toString();
        } else {
            designationStr = "";

        }


        if (netSalaryProfit.isShown()) {
            if (netSalaryProfit.getText().toString() == null
                    || netSalaryProfit.getText().toString().length() <= 0
                    || netSalaryProfit.getText().toString() == "") {
                utils.showCenterToast("Please enter netSalaryProfit ");
                isValid = false;
            } else
                netSalaryProfitStr = netSalaryProfit.getText().toString();
        } else {
            netSalaryProfitStr = "";

        }

        if (n1Name.isShown()) {
            if (n1Name.getText().toString() == null
                    || n1Name.getText().toString().length() <= 0
                    || n1Name.getText().toString() == "") {
                utils.showCenterToast("Please enter N1 Name ");
                isValid = false;
            } else
                n1NameStr = n1Name.getText().toString();
        } else {
            n1NameStr = "";

        }


        if (n2Name.isShown()) {
            if (n2Name.getText().toString() == null
                    || n2Name.getText().toString().length() <= 0
                    || n2Name.getText().toString() == "") {
                utils.showCenterToast("Please enter N2 Name");
                isValid = false;
            } else
                n2NameStr = n2Name.getText().toString();
        } else {
            n2NameStr = "";

        }


        if (cpvRemarks.isShown()) {
            if (cpvRemarks.getText().toString() == null
                    || cpvRemarks.getText().toString().length() <= 0
                    || cpvRemarks.getText().toString() == "") {
                utils.showCenterToast("Please enter CPV Remarks");
                isValid = false;
            } else
                cpvRemarksStr = cpvRemarks.getText().toString();
        } else {
            cpvRemarksStr = "";

        }

       if(meterStatusStr!=null){
           if(meterStatusStr.equalsIgnoreCase("yes")){
               if (!valid.isValidImage(meterImageBytes, "Meter Image")) {
                   return false;
               }
           }
       }


        if (!valid.isValidImage(signImageBytes, " Signature")||
                !valid.isValidImage(customerImageBytes, " Image 1")
                || !valid.isValidImage(houseImageBytes, "Image2")
                || !valid.isValidImage(ivImage_3Bytes, "Image3")
                || !valid.isValidImage(Image4ImageBytes, "Image4")
                || !valid.isValidImage(Image5ImageBytes, "Image5")) {
            return false;
        }

//		if (!valid.isValidImage(signImageBytes, "Verifier Sign Image")
//				|| !valid.isValidImage(verifierImageBytes, "Verifier Image")
//				|| !valid.isValidImage(officeImageBytes, "Office Image")) {
//			isValid = false;
//		}


        return isValid;
    }

}
