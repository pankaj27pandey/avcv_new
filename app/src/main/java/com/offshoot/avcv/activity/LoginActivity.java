package com.offshoot.avcv.activity;

import java.util.Hashtable;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.common.Validation;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.offshoot.avcv.R;
import com.offshoot.avcv.mconfig.MConfig;
import com.offshoot.avcv.parser.MParser;

public class LoginActivity extends AppCompatActivity implements
        NetworkTaskCompleteListener {

    EditText loginId, password;
    Button loginButton;
    Context mContext = LoginActivity.this;
    String imeiNumber;

    // Require Objects
    CommonUtils utils = new CommonUtils(mContext);
    Validation valid = new Validation(mContext);

    void definedVariables() {
        loginId = findViewById(R.id.loginId);
        password = findViewById(R.id.loginPassword);
        loginButton = findViewById(R.id.loginButton);
    }

    String[] checkEnteredData() {
        if (!valid.isValidField(loginId)) {
            return null;
        }
        if (!valid.isValidField(password)) {
            return null;
        }
        imeiNumber = getIMEINumber();
        String[] resultData = {loginId.getText().toString(),
                password.getText().toString(), imeiNumber};
        return resultData;
    }

    private String getIMEINumber() {
        String androidId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        return androidId;
    }

    private void sendNetworkCall(String[] values) {
        if (utils.isNetworkAvailable(mContext)) {
            Hashtable<String, String> ht = new Hashtable<String, String>();
            String[] keys = {"LoginID", "Password", "IMEINo",};
            for (int i = 0; i < keys.length; i++) {
                ht.put(keys[i], values[i]);
            }
            new NetworkTask(mContext, ht, true).execute(Urls.loginUrl);
            ht = null;
        } else {
            utils.showCenterToast(MConfig.internetMessage);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        definedVariables();

        // Event Listeners
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String[] values = checkEnteredData();
                if (values != null) {
                    sendNetworkCall(values);
                }
            }
        });
    }

    @Override
    public void onNetworkComplete(String result, String type) {
        // TODO Auto-generated method stub

        String[] resultArray = new MParser(mContext).parseXMLResponse(result);
        if (Boolean.parseBoolean(resultArray[0])) {
            // Set Login Parameters
            utils.setBoolData("LoginStatus", true);
            utils.setStringData("LoginName", resultArray[1]);
            utils.setStringData("LoginId", loginId.getText().toString());
            utils.showCenterToast("Welcome " + resultArray[1]);
            // Start Dashboard Activity
            startActivity(new Intent(mContext, MainActivity.class));
            finish();
        } else {
            //startActivity(new Intent(mContext, DashboardActivity.class));
            utils.showCenterToast(resultArray[1]);
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        utils = null;
        valid = null;
        mContext = null;
    }

}
