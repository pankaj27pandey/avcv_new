package com.offshoot.avcv.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.apzone.android.common.AppConstants;
import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.location.AddressTask;
import com.apzone.android.location.AddressTaskCompleteListener;
import com.apzone.android.location.GPSTracker;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.apzone.android.network.SyncNetworkTask;
import com.google.android.material.navigation.NavigationView;
import com.offshoot.avcv.R;
import com.offshoot.avcv.bean.DispositionBean;
import com.offshoot.avcv.bean.PaymentModeBean;
import com.offshoot.avcv.db.BackupItem;
import com.offshoot.avcv.db.DbHandler;
import com.offshoot.avcv.fragment.AboutFragment;
import com.offshoot.avcv.fragment.AllocatedCaseFragment;
import com.offshoot.avcv.fragment.CollectionAllocationFragment;
import com.offshoot.avcv.fragment.HelpFragment;
import com.offshoot.avcv.fragment.HomeFragment;
import com.offshoot.avcv.fragment.SalesFragment;
import com.offshoot.avcv.fragment.VerificationAllocationFragment;
import com.offshoot.avcv.mconfig.MConfig;
import com.offshoot.avcv.parser.MParser;
import com.offshoot.avcv.xui.AttendanceDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.List;

public class MainActivity extends AppCompatActivity


        //Note : OnFragmentInteractionListener of all the fragments
        implements
        HomeFragment.OnFragmentInteractionListener,
        AllocatedCaseFragment.OnFragmentInteractionListener,
        CollectionAllocationFragment.OnFragmentInteractionListener, SalesFragment.OnFragmentInteractionListener, VerificationAllocationFragment.OnFragmentInteractionListener,

        AboutFragment.OnFragmentInteractionListener, HelpFragment.OnFragmentInteractionListener, NavigationView.OnNavigationItemSelectedListener, NetworkTaskCompleteListener, AddressTaskCompleteListener {

    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    Context mContext;
    CommonUtils utils;
    GPSTracker tracker = GPSTracker.getInstance();
    AttendanceDialog attendDialog;
    DbHandler db;
    List<BackupItem> backupItem;
    String[] mSyncValues;
    boolean checkGPSStatus = true;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        setSupportActionBar(toolbar);
        mContext = MainActivity.this;
        utils = new CommonUtils(mContext);
        db = new DbHandler(mContext);
        attendDialog = new AttendanceDialog(mContext);
        hitServices();
        fragmentManager = getSupportFragmentManager();
        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.Open, R.string.Close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setVisible(true);
        navigationView.getMenu().findItem(R.id.nav_version).setTitle("Version :" + getVersionInfo(mContext));

        //NOTE:  Checks first item in the navigation drawer initially
        navigationView.setCheckedItem(R.id.nav_home);

        //NOTE:  Open fragment1 initially.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.mainFrame, new HomeFragment());
        ft.commit();

    }


    void hitServices() {

        if (utils.isNetworkAvailable(mContext)) {

            Hashtable<String, String> ht = new Hashtable<String, String>();
            String[] keys = {"LoginID"};
            String[] values = {utils.getStringData("LoginId")};
            for (int i = 0; i < keys.length; i++) {
                ht.put(keys[i], values[i]);
            }
            new NetworkTask(mContext, ht, true).execute(Urls.dispositionUrl);
            ht = null;


            Hashtable<String, String> ht2 = new Hashtable<String, String>();
            String[] keys1 = {"LoginID"};
            String[] values1 = {utils.getStringData("LoginId")};
            for (int i = 0; i < keys1.length; i++) {
                ht2.put(keys[i], values1[i]);
            }

            new NetworkTask(mContext, ht2, true).execute(Urls.paymentModeUrl);
            ht2 = null;


        } else {
            utils.showCenterToast(MConfig.internetMessage);
        }

    }

    void checkForGPS() {
        tracker.getLocation(this);
        if (tracker.getLatitude() == 0.0) {
            if (utils.isNetworkAvailable(mContext)) {
                tracker.showSettingsAlert("Error",
                        "Please check your GPS and Internet Settings :)");
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get longitude and latitude due to internet connection !!");
            }
        } else {
            if (utils.isNetworkAvailable(mContext)) {
                new AddressTask(this).execute(tracker.getLatitude(),
                        tracker.getLongitude());
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get location address due to internet connection !!");
            }
        }
        checkGPSStatus = false;
    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        checkForGPS();

    }

    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        utils = null;
        if (tracker != null) {
            tracker.stopUsingGPS();
        }
    }


    @Override
    public void onAddressComplete(String address) {
        // TODO Auto-generated method stub
        if (address != null) {
            tracker.setMyAddress(address);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        //NOTE: creating fragment object
        Fragment fragment = null;

        if (id == R.id.nav_home) {
            fragment = new HomeFragment();
        } else if (id == R.id.nav_markAttendance) {
            attendDialog.show(utils.getStringData("LoginName"),
                    utils.getStringData("LoginId"));
        } else if (id == R.id.nav_getAllocation) {
            AppConstants.TAG = "Allocated";
            fragment = new AllocatedCaseFragment();
        } else if (id == R.id.nav_verificationAllocation) {
            fragment = new VerificationAllocationFragment();
        } else if (id == R.id.nav_CollectionAllocation) {
            fragment = new CollectionAllocationFragment();
        } else if (id == R.id.nav_Sales) {
            fragment = new SalesFragment();
        } else if (id == R.id.nav_sync) {
            startSync();

            //   fragment = new Fragment3();
        } else if (id == R.id.nav_exit) {
            finish();
        } else if (id == R.id.nav_Logout) {
            utils.setBoolData("LoginStatus", false);
            Intent mIntent = new Intent(mContext, LoginActivity.class);
            startActivity(mIntent);
            finish();
            ///  fragment = new Fragment3();
        } else if (id == R.id.nav_about) {
            fragment = new AboutFragment();
        } else if (id == R.id.nav_help) {
            fragment = new HelpFragment();
        }
        //NOTE: Fragment changing code
        if (fragment != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.mainFrame, fragment);
            ft.commit();
        }

        //NOTE:  Closing the drawer after selecting
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String title) {
        // NOTE:  Code to replace the toolbar title based current visible fragment
        getSupportActionBar().setTitle(title);
    }

    //get the current version number and name
    public String getVersionInfo(Context context) {
        String versionName = "";
        int versionCode = -1;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }


    public void startSync() {
        backupItem = db.getAllBackupList(utils.getStringData("LoginId"));
        if (!utils.isNetworkAvailable(mContext)) {
            utils.showCenterToast(MConfig.internetMessage);
            return;
        }
        if (backupItem.size() == 0) {
            utils.showCenterToast("No data available for sync process !!");
            return;
        }
        if (tracker.getLatitude() == 0.0) {
            tracker.showSettingsAlert("Error",
                    "Please check your GPS and Internet Settings :)");
            return;
        }
        new SyncNetworkTask(mContext, backupItem, true).execute(
                tracker.getLatitude() + ", " + tracker.getLongitude(),
                tracker.getMyAddress());

    }

    void parseDisposition(String jArray) {
        JSONObject jsonObject = null;
        JSONArray jsonArray;
        db.deleteDesposition();
        try {
            jsonArray = new JSONArray(jArray);

            for (int index = 0; index < jsonArray.length(); index++) {
                jsonObject = jsonArray.getJSONObject(index);

                DispositionBean item_details = new DispositionBean();

                if (jsonObject.has("DATA_TYPE")) {
                    item_details
                            .setDATA_TYPE(jsonObject.getString("DATA_TYPE"));
                }

                if (jsonObject.has("FE_REASON1")) {
                    item_details
                            .setFE_REASON1(jsonObject.getString("FE_REASON1"));
                }

                if (jsonObject.has("FE_REASON2")) {
                    item_details
                            .setFE_REASON2(jsonObject.getString("FE_REASON2"));
                }

                if (jsonObject.has("FE_STATUS")) {
                    item_details
                            .setFE_STATUS(jsonObject.getString("FE_STATUS"));
                }

                db.insertDisposition(item_details);

            }
        } catch (Exception ee) {
            Log.e("insert Fe reason", ee.getMessage());
        }
    }

    @Override
    public void onNetworkComplete(String result, String type) {


        // TODO Auto-generated method stub
        String[] resultArray = new MParser(mContext).parseXMLResponse(result);
        if (type.equals(Urls.dispositionUrl)) {
            if (Boolean.parseBoolean(resultArray[0])) {
                parseDisposition(resultArray[1]);
            } else {
                utils.showCenterToast(resultArray[1]);
            }

        }
        if (type.equals(Urls.attendanceUrl)) {
            if (Boolean.parseBoolean(resultArray[0])) {
                attendDialog.hide();
                utils.showCenterToast(resultArray[1]);
            } else {
                utils.showCenterToast(resultArray[1]);
            }

        }
        if (type.equals(Urls.paymentModeUrl)) {
            if (Boolean.parseBoolean(resultArray[0])) {
                parsePaymentMode(resultArray[1]);
            } else {
                utils.showCenterToast(resultArray[1]);
            }
        }
    }


    void parsePaymentMode(String jArray) {
        JSONObject jsonObject = null;
        JSONArray jsonArray;
        db.deletePaymentMode();
        try {
            jsonArray = new JSONArray(jArray);

            for (int index = 0; index < jsonArray.length(); index++) {
                jsonObject = jsonArray.getJSONObject(index);

                PaymentModeBean item_details = new PaymentModeBean();

                if (jsonObject.has("PaymentMode")) {
                    item_details
                            .setPaymentMode(jsonObject.getString("PaymentMode"));
                }


                db.insertPaymentMode(item_details);

            }
        } catch (Exception ee) {
            Log.e("insert Fe reason", ee.getMessage());
        }
    }


}
