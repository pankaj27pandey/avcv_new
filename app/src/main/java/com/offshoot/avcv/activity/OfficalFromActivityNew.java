package com.offshoot.avcv.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.common.Validation;
import com.apzone.android.common.XUI;
import com.apzone.android.crop.Crop;
import com.apzone.android.location.AddressTask;
import com.apzone.android.location.AddressTaskCompleteListener;
import com.apzone.android.location.GPSTracker;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.offshoot.avcv.R;
import com.offshoot.avcv.db.BackupItem;
import com.offshoot.avcv.db.DbHandler;
import com.offshoot.avcv.mconfig.MConfig;
import com.offshoot.avcv.parser.MParser;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Hashtable;

import static com.apzone.android.common.AppConstants.SIGNATURE_ACTIVITY;

public class OfficalFromActivityNew extends Activity implements
        AddressTaskCompleteListener, NetworkTaskCompleteListener {

    ImageView navbarBackIcon;
    LinearLayout navbarBack;
    TextView title;
    Calendar c = Calendar.getInstance();

    void definedAndSetUI() {
        navbarBackIcon = (ImageView) findViewById(R.id.navbarBackIcon);
        navbarBack = (LinearLayout) findViewById(R.id.navbarBack);
        title = (TextView) findViewById(R.id.navbarTitle);
        title.setText(getString(R.string.office_form));
        navbarBackIcon.setVisibility(View.VISIBLE);
        navbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

    Context mContext = OfficalFromActivityNew.this;
    CommonUtils utils = new CommonUtils(mContext);
    XUI xui = new XUI(mContext);
    DbHandler db = new DbHandler(mContext);
    GPSTracker tracker = GPSTracker.getInstance();
    Validation valid;
    String[] enteredValues;

    Spinner officeAccessibilitySpinner, typeOfEmploymentSpinner,
            officeStatusSpinner, boardSeenSpinner, setupSeenSpinner, n1StatusSpinner, n2StatusSpinner,meterStatusSpinner, avcvStatusSpinner,
            negativeReasonSpinner;
    EditText personMeetEditText, etNatureOfBusiness, etDesignation,
            personDesignationEditText, etNetSalaryProfit,
            n1Name, n2Name, workingSince,
            roadmapLandmark, cpvRemarks;
    TextView locationAddress, serviceProviderTypeText, verifierName, tvOfficeStatus, tvAvcvReason;
    ImageView verifierSignImage, verifierImage, officeImage, houseImage,meterImage,image4Image,image5Image;
    Button submitButton;
    ScrollView officeActivityScrollView;

    String AVCVDetailsID;
    String mLocationAddress;
    int imageType = 0;
    String signImageBytes = null;
    String verifierImageBytes = null;
    String officeImageBytes = null;
    String houseImageBytes = null;
    String meterImageBytes = null;
    String Image4ImageBytes = null;
    String Image5ImageBytes = null;
    byte[] signImageBlobData = null;
    byte[] userImageBlobData = null;
    byte[] placeImageBlobData = null;
    byte[] houseImageBlobData = null;
    byte[] meterImageBlobData = null;
    byte[] image4BlobData = null;
    byte[] image5BlobData = null;

    String accessibilityStr, personMeetStr, personDesignationStr, typeOfEmploymentStr, natureOfBusinessStr, designationStr, officeStatusStr,
            officeBoardSeenStr, officeSetupStr, workingSinceStr, netSalaryProfitStr, n1NameStr, n1StatusStr, n2NameStr, n2StatusStr,
            roadMapLandmarkStr,meterStatusStr, avcvStatusStr, negativeReasonStr, cpvRemarksStr;

    String[] checkEnteredData() {




        String[] mValues = {utils.getStringData("LoginId"),AVCVDetailsID,
                accessibilityStr, personMeetStr, personDesignationStr, typeOfEmploymentStr, natureOfBusinessStr, designationStr,
                officeStatusStr, officeBoardSeenStr, officeSetupStr, workingSince.getText().toString(),
                netSalaryProfitStr, n1NameStr, n1StatusStr, n2NameStr, n2StatusStr,
                verifierName.getText().toString(), roadmapLandmark.getText().toString(),
            meterStatusStr, avcvStatusStr, negativeReasonStr, cpvRemarksStr,
                tracker.getLatitude() + ", " + tracker.getLongitude(), mLocationAddress,
                /*signImageBytes,*/meterImageBytes, verifierImageBytes, officeImageBytes, houseImageBytes,Image4ImageBytes,Image5ImageBytes};

        return mValues;
    }

    void defiendVaribles() {
        officeAccessibilitySpinner = (Spinner) findViewById(R.id.officeAccessibilitySpinner);
        typeOfEmploymentSpinner = (Spinner) findViewById(R.id.typeOfEmploymentSpinner);
        officeStatusSpinner = (Spinner) findViewById(R.id.officeStatusSpinner);
        boardSeenSpinner = (Spinner) findViewById(R.id.officeBoardSeenSpinner);
        setupSeenSpinner = (Spinner) findViewById(R.id.officeSetupSpinner);
        n1StatusSpinner = (Spinner) findViewById(R.id.n1StatusSpinner);
        n2StatusSpinner = (Spinner) findViewById(R.id.n2StatusSpinner);
        avcvStatusSpinner = (Spinner) findViewById(R.id.avcvStatusSpinner);
        meterStatusSpinner= (Spinner) findViewById(R.id.meterStatusSpinner);
        negativeReasonSpinner = (Spinner) findViewById(R.id.negativeReasonSpinner);

        personMeetEditText = (EditText) findViewById(R.id.personMeetEditText);
        personDesignationEditText = (EditText) findViewById(R.id.personDesignationEditText);
        n1Name = (EditText) findViewById(R.id.n1Name);
        n2Name = (EditText) findViewById(R.id.n2Name);
        roadmapLandmark = (EditText) findViewById(R.id.roadmapLandmark);
        cpvRemarks = (EditText) findViewById(R.id.cpvRemarks);
        etNatureOfBusiness = (EditText) findViewById(R.id.etNatureOfBusiness);
        etDesignation = (EditText) findViewById(R.id.etDesignation);
        workingSince = (EditText) findViewById(R.id.workingSince);
        etNetSalaryProfit = (EditText) findViewById(R.id.netSalaryProfit);


        verifierSignImage = (ImageView) findViewById(R.id.verifierSignImage);
        image4Image = (ImageView) findViewById(R.id.Image4);
        image5Image = (ImageView) findViewById(R.id.Image5);
        meterImage = (ImageView) findViewById(R.id.meterImage);
        verifierImage = (ImageView) findViewById(R.id.verifierImage);
        officeImage = (ImageView) findViewById(R.id.officeImage);
        houseImage = (ImageView) findViewById(R.id.houseImage);

        verifierName = (TextView) findViewById(R.id.verifierName);
        locationAddress = (TextView) findViewById(R.id.locationAddress);
        tvOfficeStatus = (TextView) findViewById(R.id.tvOfficeStatus);
        tvAvcvReason = (TextView) findViewById(R.id.tvAvcvReason);
        submitButton = (Button) findViewById(R.id.submitButton);
        officeActivityScrollView = (ScrollView) findViewById(R.id.officeActivityScrollView);
    }

    void addEventListeners() {

        workingSince.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showDialog(0);
            }
        });

        officeStatusSpinner
                .setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View mView,
                                               int position, long arg3) {
//                        if (position == 3) {
//                            rentPaidEditText.setVisibility(View.VISIBLE);
//                        } else {
//                            rentPaidEditText.setVisibility(View.GONE);
//                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });


        typeOfEmploymentSpinner
                .setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View mView,
                                               int position, long arg3) {
                        String selectedItem = parent
                                .getItemAtPosition(position).toString();
                        if (selectedItem.equals("Own Business")) {
                            etDesignation.setVisibility(View.GONE);
                            etNatureOfBusiness.setVisibility(View.VISIBLE);
                            tvOfficeStatus.setVisibility(View.VISIBLE);
                            officeStatusSpinner.setVisibility(View.VISIBLE);
                        } else if (selectedItem.equalsIgnoreCase("Govt. Job") || selectedItem.equalsIgnoreCase("Pvt Job")) {
                            etDesignation.setVisibility(View.VISIBLE);
                            etNatureOfBusiness.setVisibility(View.GONE);
                            tvOfficeStatus.setVisibility(View.GONE);
                            officeStatusSpinner.setVisibility(View.GONE);
                        } else {
                            etDesignation.setVisibility(View.GONE);
                            etNatureOfBusiness.setVisibility(View.GONE);
                            tvOfficeStatus.setVisibility(View.GONE);
                            officeStatusSpinner.setVisibility(View.GONE);

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });
        avcvStatusSpinner
                .setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View mView,
                                               int position, long arg3) {
                        String selectedItem = parent
                                .getItemAtPosition(position).toString();
                        if (selectedItem.equals("Negative")) {
                            tvAvcvReason.setVisibility(View.VISIBLE);
                            negativeReasonSpinner.setVisibility(View.VISIBLE);

                        } else {
                            negativeReasonSpinner.setVisibility(View.GONE);
                            tvAvcvReason.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub

                    }

                });

        verifierSignImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
//				imageType = 1;
//				Crop.pickImage((Activity) mContext, 2);
                // TODO Auto-generated method stub
                Intent intent = new Intent(mContext, CaptureSignatureActivity.class);
                startActivityForResult(intent, SIGNATURE_ACTIVITY);
            }
        });
        meterImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imageType = 2;
                xui.createImagePickOptionDialog();
            }
        });
        verifierImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                imageType = 3;
                xui.createImagePickOptionDialog();
            }
        });
        officeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 4;
                xui.createImagePickOptionDialog();
            }
        });
        houseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 5;
                xui.createImagePickOptionDialog();
            }
        });

        image4Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 6;
                xui.createImagePickOptionDialog();
            }
        });
        image5Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 7;
                xui.createImagePickOptionDialog();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (validate()) {
                    enteredValues = checkEnteredData();
                    if (enteredValues != null) {
                        if (utils.isNetworkAvailable(mContext)) {
                            Hashtable<String, String> ht = new Hashtable<String, String>();
                            String[] keys = Urls.OfficeFormkeys;
                            for (int i = 0; i < keys.length; i++) {
                                ht.put(keys[i], enteredValues[i] + "");
                            }
                            new NetworkTask(mContext, ht, true)
                                    .execute(Urls.submitOfficeForm);
                            ht = null;
                        } else {
                            try {
                                int mLength = enteredValues.length;
                                enteredValues[mLength - 3] = "SavedSignImage";
                                enteredValues[mLength - 2] = "SavedUserImage";
                                enteredValues[mLength - 1] = "SavedPlaceImage";
                                BackupItem mItem = new BackupItem();
                                mItem.setUserId(utils.getStringData("LoginId"));
                                mItem.setAvcvId(AVCVDetailsID);
                                mItem.setAvcvData(utils
                                        .convertArrayToString(enteredValues));
                                mItem.setType("office");
                                mItem.setSignImage(signImageBlobData);
                                mItem.setUserImage(userImageBlobData);
                                mItem.setPlaceImage(placeImageBlobData);
                                db.insertIntoBackUp(mItem);
                                db.deleteFromCases(AVCVDetailsID);
                                finish();
                                utils.showCenterToast(MConfig.internetMessage
                                        + "\nYour data has been saved in local storage!!");

                            } catch (Exception ee) {
                                utils.showCenterToast("Unable to save data" + "\n"
                                        + ee.getMessage());
                            }
                        }
                    } else {
                        utils.showCenterToast("Please enter all the required values!!");
                    }
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offical_form_new);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        AVCVDetailsID = getIntent().getExtras().getString("VerificationID");
        definedAndSetUI();
        defiendVaribles();
        addEventListeners();
        valid = new Validation(mContext, officeActivityScrollView);
        verifierName.setText(utils.getStringData("LoginName"));

    }

    boolean checkGPSStatus = true;

    void checkForGPS() {
        tracker.getLocation(this);
        if (tracker.getLatitude() == 0.0) {
            if (utils.isNetworkAvailable(mContext)) {
                tracker.showSettingsAlert("Error",
                        "Please check your GPS and Internet Settings :)");
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get longitude and latitude due to internet connection !!");
            }
        } else {
            if (utils.isNetworkAvailable(mContext)) {
                new AddressTask(this).execute(tracker.getLatitude(),
                        tracker.getLongitude());
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get location address due to internet connection !!");
            }
        }
        checkGPSStatus = false;
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        checkForGPS();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent result) {
        ImageView resultImageView = null;
        byte[] imageArray = null;
        if (resultCode == RESULT_OK)
            if (requestCode == SIGNATURE_ACTIVITY) {
                imageArray = result.getByteArrayExtra("byteArray");
                String base64StringImage = Base64.encodeToString(imageArray, 0);
                signImageBlobData = imageArray;
                resultImageView = verifierSignImage;
                signImageBytes = base64StringImage;
            } else {
                imageArray = Crop.getImageBitmap(requestCode, resultCode,
                        result, (Activity) mContext, false);
                if (imageArray != null) {
                    String base64StringImage = Base64.encodeToString(
                            imageArray, 0);
                    switch (imageType) {
                        case 2:
                            meterImageBlobData = imageArray;
                            resultImageView = meterImage;
                            meterImageBytes = base64StringImage;
                            break;
                        case 3:
                            userImageBlobData = imageArray;
                            resultImageView = verifierImage;
                            verifierImageBytes = base64StringImage;
                            break;
                        case 4:
                            placeImageBlobData = imageArray;
                            resultImageView = officeImage;
                            officeImageBytes = base64StringImage;

                            break;
                        case 5:
                            houseImageBlobData = imageArray;
                            resultImageView = houseImage;
                            houseImageBytes = base64StringImage;
                            break;
                        case 6:
                            image4BlobData = imageArray;
                            resultImageView = image4Image;
                            Image4ImageBytes = base64StringImage;
                            break;
                        case 7:
                            image5BlobData = imageArray;
                            resultImageView = image5Image;
                            Image5ImageBytes = base64StringImage;
                            break;
                    }

                }

            }
        if (resultImageView != null) {

            resultImageView.setImageDrawable(null);
            Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0,
                    imageArray.length);
            resultImageView.setImageBitmap(bmp);
//			resultImageView.setImageDrawable(null);
//			Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0,
//					imageArray.length);
//			Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);
//
//
//			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system
//
//			Canvas cs = new Canvas(mutableBitmap);
//			Paint tPaint = new Paint();
//			tPaint.setTextSize(35);
//			tPaint.setColor(Color.RED);
//			tPaint.setStyle(Paint.Style.FILL);
//			cs.drawBitmap(mutableBitmap, 0f, 0f, null);
//			float height = tPaint.measureText("yY");
//			cs.drawText(dateTime, 20f, height + 15f, tPaint);
//			resultImageView.setImageBitmap(mutableBitmap);
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        xui = null;
        utils = null;
        valid = null;
        db = null;
        if (tracker != null) {
            tracker.stopUsingGPS();
        }
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                DatePickerDialog dpd = new DatePickerDialog(mContext,
                        pickerListener, c.get(Calendar.YEAR),
                        c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                dpd.setTitle("Month and Year");
                try {

                    Field[] datePickerDialogFields = dpd.getClass()
                            .getDeclaredFields();
                    for (Field datePickerDialogField : datePickerDialogFields) {
                        if (datePickerDialogField.getName().equals("mDatePicker")) {
                            datePickerDialogField.setAccessible(true);
                            DatePicker datePicker = (DatePicker) datePickerDialogField
                                    .get(dpd);
                            Field datePickerFields[] = datePickerDialogField
                                    .getType().getDeclaredFields();
                            for (Field datePickerField : datePickerFields) {
                                if ("mDayPicker".equals(datePickerField.getName())
                                        || "mDaySpinner".equals(datePickerField
                                        .getName())) {
                                    datePickerField.setAccessible(true);
                                    Object dayPicker = new Object();
                                    dayPicker = datePickerField.get(datePicker);
                                    ((View) dayPicker).setVisibility(View.GONE);
                                }
                            }
                        }

                    }
                } catch (Exception ex) {
                }
                return dpd;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub

            // Show selected date
            StringBuilder selectedDate = new StringBuilder()
                    .append(monthOfYear + 1).append("-").append(year)
                    .append(" ");

            workingSince.setText(selectedDate);

        }

    };


    @Override
    public void onAddressComplete(String address) {
        // TODO Auto-generated method stub
        if (address != null) {
            mLocationAddress = address;
            locationAddress.setText(tracker.getLatitude() + ", "
                    + tracker.getLongitude() + "\n" + mLocationAddress);
        }
    }

    @Override
    public void onNetworkComplete(String result, String type) {
        // TODO Auto-generated method stub
        String[] resultArray = new MParser(mContext).parseXMLResponse(result);
        if (Boolean.parseBoolean(resultArray[0])) {
            db.deleteFromCases(AVCVDetailsID);
            finish();
        }
        utils.showCenterToast(resultArray[1]);
    }

    private boolean validate() {
        boolean isValid = true;
        if (officeAccessibilitySpinner.isShown()) {
            if (officeAccessibilitySpinner.getSelectedItem().toString() == null
                    || officeAccessibilitySpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Accessibility ");
                isValid = false;
            } else
                accessibilityStr = officeAccessibilitySpinner.getSelectedItem().toString();
        } else {

            accessibilityStr = "";
        }


        if (typeOfEmploymentSpinner.isShown()) {
            if (typeOfEmploymentSpinner.getSelectedItem().toString() == null
                    || typeOfEmploymentSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Type Of Employment ");
                isValid = false;
            } else
                typeOfEmploymentStr = typeOfEmploymentSpinner.getSelectedItem().toString();
        } else {

            typeOfEmploymentStr = "";
        }
        if (officeStatusSpinner.isShown()) {
            if (officeStatusSpinner.getSelectedItem().toString() == null
                    || officeStatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select office status");
                isValid = false;
            } else
                officeStatusStr = officeStatusSpinner.getSelectedItem().toString();
        } else {

            officeStatusStr = "";
        }

        if (boardSeenSpinner.isShown()) {
            if (boardSeenSpinner.getSelectedItem().toString() == null
                    || boardSeenSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Business/office Board seen");
                isValid = false;
            } else
                officeBoardSeenStr = boardSeenSpinner.getSelectedItem().toString();
        } else {

            officeBoardSeenStr = "";
        }

        if (setupSeenSpinner.isShown()) {
            if (setupSeenSpinner.getSelectedItem().toString() == null
                    || setupSeenSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Business/office Setup  ");
                isValid = false;
            } else
                officeSetupStr = setupSeenSpinner.getSelectedItem().toString();
        } else {

            officeSetupStr = "";
        }


        if (n1StatusSpinner.isShown()) {
            if (n1StatusSpinner.getSelectedItem().toString() == null
                    || n1StatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select N1 Status ");
                isValid = false;
            } else
                n1StatusStr = n1StatusSpinner.getSelectedItem().toString();
        } else {

            n1StatusStr = "";
        }//
        if (n2StatusSpinner.isShown()) {
            if (n2StatusSpinner.getSelectedItem().toString() == null
                    || n2StatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select N2 Status ");
                isValid = false;
            } else
                n2StatusStr = n2StatusSpinner.getSelectedItem().toString();
        } else {

            n2StatusStr = "";
        }
        if (meterStatusSpinner.isShown()) {
            if (meterStatusSpinner.getSelectedItem().toString() == null
                    || meterStatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Meter Status ");
                isValid = false;
            } else
                meterStatusStr = meterStatusSpinner.getSelectedItem().toString();
        } else {

            meterStatusStr = "";
        }

        if (avcvStatusSpinner.isShown()) {
            if (avcvStatusSpinner.getSelectedItem().toString() == null
                    || avcvStatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Avcv Status ");
                isValid = false;
            } else
                avcvStatusStr = avcvStatusSpinner.getSelectedItem().toString();
        } else {

            avcvStatusStr = "";
        }
        if (negativeReasonSpinner.isShown()) {
            if (negativeReasonSpinner.getSelectedItem().toString() == null
                    || negativeReasonSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Negative Reason  ");
                isValid = false;
            } else
                negativeReasonStr = negativeReasonSpinner.getSelectedItem().toString();
        } else {

            negativeReasonStr = "";
        }
//        if (nextAppointmentTv.isShown()) {
//            if (nextAppointmentTv.getText().toString() == null
//                    || nextAppointmentTv.getText().toString().length() <= 0
//                    || nextAppointmentTv.getText().toString() == "") {
//                utils.showCenterToast("Please enter next appointment date ");
//                isValid = false;
//            } else if (nextAppointmentTimeTv.getText().toString() == null
//                    || nextAppointmentTimeTv.getText().toString() == ""
//                    || nextAppointmentTimeTv.getText().toString().length() <= 0) {
//                utils.showCenterToast("Please enter next appointment time  ");
//                isValid = false;
//            } else {
//                String minuString = "";
//                String hourString = "";
//                if (minute < 10)
//                    minuString = "0" + minute;
//                else
//                    minuString = String.valueOf(minute);
//                if (hour < 10)
//                    hourString = "0" + hour;
//                else
//                    hourString = String.valueOf(hour);
//
//                nextAppString = nextAppointmentTv.getText().toString()
//                        + hourString + minuString;
//            }
//        } else {
//            nextAppString = "";
//        }

        if (personMeetEditText.isShown()) {
            if (personMeetEditText.getText().toString() == null
                    || personMeetEditText.getText().toString().length() <= 0
                    || personMeetEditText.getText().toString() == "") {
                utils.showCenterToast("Please enter Person Meet ");
                isValid = false;
            } else
                personMeetStr = personMeetEditText.getText().toString();
        } else {
            personMeetStr = "";
        }


        if (personDesignationEditText.isShown()) {
            if (personDesignationEditText.getText().toString() == null
                    || personDesignationEditText.getText().toString().length() <= 0
                    || personDesignationEditText.getText().toString() == "") {
                utils.showCenterToast("Please enter Person Designation Meet ");
                isValid = false;
            } else
                personDesignationStr = personDesignationEditText.getText().toString();
        } else {
            personDesignationStr = "";
        }

        if (etNatureOfBusiness.isShown()) {
            if (etNatureOfBusiness.getText().toString() == null
                    || etNatureOfBusiness.getText().toString().length() <= 0
                    || etNatureOfBusiness.getText().toString() == "") {
                utils.showCenterToast("Please enter Employment Details ");
                isValid = false;
            } else
                natureOfBusinessStr = etNatureOfBusiness.getText().toString();
        } else {
            natureOfBusinessStr = "";

        }

        if (etDesignation.isShown()) {
            if (etDesignation.getText().toString() == null
                    || etDesignation.getText().toString().length() <= 0
                    || etDesignation.getText().toString() == "") {
                utils.showCenterToast("Please enter Designation ");
                isValid = false;
            } else
                designationStr = etDesignation.getText().toString();
        } else {
            designationStr = "";

        }

        if (etNetSalaryProfit.isShown()) {
            if (etNetSalaryProfit.getText().toString() == null
                    || etNetSalaryProfit.getText().toString().length() <= 0
                    || etNetSalaryProfit.getText().toString() == "") {
                utils.showCenterToast("Please enter netSalaryProfit ");
                isValid = false;
            } else
                netSalaryProfitStr = etNetSalaryProfit.getText().toString();
        } else {
            netSalaryProfitStr = "";

        }

        if (n1Name.isShown()) {
            if (n1Name.getText().toString() == null
                    || n1Name.getText().toString().length() <= 0
                    || n1Name.getText().toString() == "") {
                utils.showCenterToast("Please enter N1 Name ");
                isValid = false;
            } else
                n1NameStr = n1Name.getText().toString();
        } else {
            n1NameStr = "";

        }


        if (n2Name.isShown()) {
            if (n2Name.getText().toString() == null
                    || n2Name.getText().toString().length() <= 0
                    || n2Name.getText().toString() == "") {
                utils.showCenterToast("Please enter N2 Name");
                isValid = false;
            } else
                n2NameStr = n2Name.getText().toString();
        } else {
            n2NameStr = "";

        }


        if (cpvRemarks.isShown()) {
            if (cpvRemarks.getText().toString() == null
                    || cpvRemarks.getText().toString().length() <= 0
                    || cpvRemarks.getText().toString() == "") {
                utils.showCenterToast("Please enter CPV Remarks");
                isValid = false;
            } else
                cpvRemarksStr = cpvRemarks.getText().toString();
        } else {
            cpvRemarksStr = "";

        }



//		if (!valid.isValidImage(signImageBytes, "Verifier Sign Image")
//				|| !valid.isValidImage(verifierImageBytes, "Verifier Image")
//				|| !valid.isValidImage(officeImageBytes, "Office Image")) {
//			isValid = false;
//		}

        if(meterStatusStr.equalsIgnoreCase("yes")){
            if (!valid.isValidImage(verifierImageBytes, "Meter Image")) {
                return false;
            }
        }
        if (!valid.isValidImage(verifierImageBytes, " Image 1")
                || !valid.isValidImage(officeImageBytes, "Image2")
                || !valid.isValidImage(houseImageBytes, "Image3")
                || !valid.isValidImage(Image4ImageBytes, "Image4")
                || !valid.isValidImage(Image5ImageBytes, "Image5")) {
            return false;
        }


        return isValid;
    }
}
