package com.offshoot.avcv.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.apzone.android.common.Validation;
import com.apzone.android.common.XUI;
import com.apzone.android.crop.Crop;
import com.apzone.android.location.AddressTask;
import com.apzone.android.location.AddressTaskCompleteListener;
import com.apzone.android.location.GPSTracker;
import com.apzone.android.network.NetworkTask;
import com.apzone.android.network.NetworkTaskCompleteListener;
import com.offshoot.avcv.R;
import com.offshoot.avcv.bean.DispositionBean;
import com.offshoot.avcv.db.BackupItem;
import com.offshoot.avcv.db.DbHandler;
import com.offshoot.avcv.listadapters.CustomAdapter;
import com.offshoot.avcv.mconfig.MConfig;
import com.offshoot.avcv.parser.MParser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

public class CollectionActivity extends AppCompatActivity implements
        AddressTaskCompleteListener, NetworkTaskCompleteListener {
    public static final int SIGNATURE_ACTIVITY = 100;
    static final int TIME_DIALOG_ID = 1111;
    ImageView navbarBackIcon;
    LinearLayout navbarBack;
    TextView title;
    Context mContext = CollectionActivity.this;
    CommonUtils utils = new CommonUtils(mContext);
    XUI xui = new XUI(mContext);
    DbHandler db = new DbHandler(mContext);
    GPSTracker tracker = GPSTracker.getInstance();
    Validation valid;
    String[] enteredValues;
    SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMyyyy", Locale.US);
    Spinner reason2Spinner, fe_statusSpinner, cheque_typeSpinner, payment_modeSpinner, reasonSpinner, contactStatusSpinner, pdaTypeSpinner;
    EditText etContactPersonName, etContactPersonNumber, tc_remarksEditText, cheque_no_et, amount_et, bankname_et, tcfeedback_et;
    TextView tv_reason2, locationAddress, nextAppointmentTv, nextAppointmentDateTextTv, nextAppointmentTimeTv, cycle_tv, chequedate_tv,
            chequetype_tv, tv_paymentMode, tv_reason, tvContactStatus;
    ImageView verifierSignImage, verifierImage, officeImage;
    Button submitButton;
    ScrollView officeActivityScrollView;
    String type, dataType, AVCVDetailsID, nextAppString, tc_remartstr, fe_statusstr, Cyclestr, cheque_typestr, cheque_no_str, banknamestr,
            amountstr, chequedatestr, tcFeedback_str, picLatLongstr, picLocationstr, fe_reasonstr, paymentModestr,
            pdaTypestr, contactStatusStr, feReason2, contactPersonNameStr = "", contactPersonNumberStr = "";
    String mLocationAddress;
    int imageType = 0;
    String signImageBytes = null;
    String verifierImageBytes = null;
    String officeImageBytes = null;
    byte[] signImageBlobData = null;
    byte[] userImageBlobData = null;
    byte[] placeImageBlobData = null;
    List<DispositionBean> feReasonBeansList;
    List<String> reasonList, paymentModeList, reason1List, reason2List, actualReason2List = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ArrayAdapter<DispositionBean> customAdapter;
    CustomAdapter customAdapter1;
    boolean checkGPSStatus = true;
    private int hour;
    private int minute;
    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minutes) {
            // TODO Auto-generated method stub
            hour = hourOfDay;
            minute = minutes;

            updateTime(hour, minute);

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.activity_collection_form);
        AVCVDetailsID = getIntent().getExtras().getString("AVCVDetailsID");
        Cyclestr = getIntent().getExtras().getString("CYCLE");
        dataType = this.getIntent().getExtras().getString("DATATYPE");
        definedAndSetUI(dataType);
        defiendVaribles();
        addEventListeners();
        valid = new Validation(mContext, officeActivityScrollView);
    }

    void definedAndSetUI(String dataType) {
        navbarBackIcon = findViewById(R.id.navbarBackIcon);
        navbarBack = findViewById(R.id.navbarBack);
        title = findViewById(R.id.navbarTitle);

        title.setText("Trace Form");


        navbarBackIcon.setVisibility(View.VISIBLE);
        navbarBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                finish();
            }
        });
    }

    String[] checkEnteredData() {
        String[] mValues = {AVCVDetailsID, type, fe_statusstr, feReason2, tc_remartstr, nextAppString,
                pdaTypestr, paymentModestr, amountstr,
                cheque_typestr, cheque_no_str, chequedatestr, banknamestr, tcFeedback_str, contactStatusStr, contactPersonNameStr, contactPersonNumberStr,
                tracker.getLatitude() + ", " + tracker.getLongitude(), mLocationAddress, picLatLongstr,
                picLocationstr, signImageBytes, verifierImageBytes, officeImageBytes};

        return mValues;
    }

    void defiendVaribles() {

        cycle_tv = findViewById(R.id.cycle_tv);
        cycle_tv.setText(String.format("CYCLE            %s", Cyclestr));
        fe_statusSpinner = findViewById(R.id.fe_statusSpinner);
        reason2Spinner = findViewById(R.id.reason2Spinner);
        tv_reason2 = findViewById(R.id.tv_reason2);
        cheque_typeSpinner = findViewById(R.id.cheque_typeSpinner);
        contactStatusSpinner = findViewById(R.id.contact_statusSpinner);
        tvContactStatus = findViewById(R.id.tvContactStatus);
        pdaTypeSpinner = findViewById(R.id.fe_typeSpinner);

        cheque_no_et = findViewById(R.id.cheque_numberEditText);
        amount_et = findViewById(R.id.amountEditText);
        chequedate_tv = findViewById(R.id.tv_cheque_date);
        chequetype_tv = findViewById(R.id.cheque_typetv);
        bankname_et = findViewById(R.id.bankEditText);
        tcfeedback_et = findViewById(R.id.tc_feedbackEditText);

        payment_modeSpinner = findViewById(R.id.payment_modeSpinner);
        tv_paymentMode = findViewById(R.id.tv_paymentMode);

        reasonSpinner = findViewById(R.id.reasonSpinner);
        tv_reason = findViewById(R.id.tv_reason);

        tc_remarksEditText = findViewById(R.id.tc_remarksEditText);
        etContactPersonName = findViewById(R.id.etContactPersonName);
        etContactPersonNumber = findViewById(R.id.etContactPersonNumber);

        nextAppointmentTv = findViewById(R.id.tv_next_appointment_date);
        nextAppointmentTimeTv = findViewById(R.id.tv_next_appointment_time);
        nextAppointmentDateTextTv = findViewById(R.id.tv_next_appointment_date_text);


        verifierSignImage = findViewById(R.id.verifierSignImage);
        verifierImage = findViewById(R.id.verifierImage);
        officeImage = findViewById(R.id.officeImage);

        locationAddress = findViewById(R.id.locationAddress);

        submitButton = findViewById(R.id.submitButton);
        officeActivityScrollView = findViewById(R.id.officeActivityScrollView);


        type = "TRACEABLE";


        feReasonBeansList = db.getFeReason(type, "COL");

        // Resources passed to adapter to get image
        Resources res = getResources();

        // Create custom adapter object ( see below CustomAdapter.java )
        customAdapter1 = new CustomAdapter(mContext, R.layout.spinner_row, (ArrayList) feReasonBeansList, res);
        customAdapter1.setDropDownViewResource(R.layout.spinner_row);
        fe_statusSpinner.setAdapter(customAdapter1);


    }

    private void showDatePickerDialog(final TextView text) {
        Calendar newCalendar = Calendar.getInstance();

        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(
                CollectionActivity.this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                text.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.show();
    }

    void addEventListeners() {

        nextAppointmentTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog(nextAppointmentTv);
            }
        });

        nextAppointmentTimeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(TIME_DIALOG_ID);
            }
        });
        fe_statusSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {

                        DispositionBean feReasonBean = (DispositionBean) parent.getSelectedItem();


                        if (feReasonBean.getFE_REASON2().isEmpty()) {
                            reason2Spinner.setVisibility(View.GONE);
                            tv_reason2.setVisibility(View.GONE);
                        } else {
                            reason2Spinner.setVisibility(View.VISIBLE);
                            tv_reason2.setVisibility(View.VISIBLE);
                            actualReason2List = new ArrayList<>(Arrays.asList(feReasonBean.getFE_REASON2().split("/")));
                            actualReason2List.add(0, "Select");
                            adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, actualReason2List);
                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            reason2Spinner.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }

                        String selectedItem = feReasonBean.getFE_REASON1();
                        if (selectedItem.equals("PAID")) {
                            etContactPersonNumber.setVisibility(View.GONE);
                            etContactPersonName.setVisibility(View.GONE);
                            //  amount_et.setVisibility(View.VISIBLE);
                            nextAppointmentDateTextTv.setVisibility(View.GONE);
                            nextAppointmentTimeTv.setVisibility(View.GONE);
                            if (Cyclestr.equals("SPLN")) {
                                nextAppointmentTv.setVisibility(View.GONE);

                                cheque_typeSpinner.setVisibility(View.VISIBLE);
                                chequetype_tv.setVisibility(View.VISIBLE);
                                payment_modeSpinner.setVisibility(View.GONE);
                                tv_paymentMode.setVisibility(View.GONE);
                            } else {
                                amount_et.setVisibility(View.VISIBLE);
                                payment_modeSpinner.setVisibility(View.VISIBLE);
                                tv_paymentMode.setVisibility(View.VISIBLE);
                                paymentModeList = db.getAllPaymentMode();
                                paymentModeList.add(0, "Select");
                                adapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_item, paymentModeList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                payment_modeSpinner.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                            }
                            reasonSpinner.setVisibility(View.GONE);
                            tv_reason.setVisibility(View.GONE);


                        } else if (selectedItem.equals("NOT PAID")) {

                            nextAppointmentTv.setVisibility(View.GONE);
                            nextAppointmentDateTextTv.setVisibility(View.GONE);
                            nextAppointmentTimeTv.setVisibility(View.GONE);
                            cheque_typeSpinner.setVisibility(View.GONE);
                            chequetype_tv.setVisibility(View.GONE);
                            cheque_no_et.setVisibility(View.GONE);
                            chequedate_tv.setVisibility(View.GONE);
                            bankname_et.setVisibility(View.GONE);
                            amount_et.setVisibility(View.GONE);
                            payment_modeSpinner.setVisibility(View.GONE);
                            tv_paymentMode.setVisibility(View.GONE);

                            reasonSpinner.setVisibility(View.GONE);
                            tv_reason.setVisibility(View.GONE);
                            tv_reason.setText("FE_REASON");


                        } else if (selectedItem.equals("TC HELP")) {

                            nextAppointmentTv.setVisibility(View.GONE);
                            nextAppointmentDateTextTv.setVisibility(View.GONE);
                            nextAppointmentTimeTv.setVisibility(View.GONE);
                            cheque_typeSpinner.setVisibility(View.GONE);
                            chequetype_tv.setVisibility(View.GONE);
                            cheque_no_et.setVisibility(View.GONE);
                            chequedate_tv.setVisibility(View.GONE);
                            bankname_et.setVisibility(View.GONE);
                            amount_et.setVisibility(View.GONE);
                            payment_modeSpinner.setVisibility(View.GONE);
                            tv_paymentMode.setVisibility(View.GONE);

                            reasonSpinner.setVisibility(View.GONE);
                            tv_reason.setVisibility(View.GONE);
                            tv_reason.setText("FE_REASON");


                        } else if (selectedItem.equals("FOLLOW UP")) {

                            nextAppointmentTv.setVisibility(View.VISIBLE);
                            nextAppointmentDateTextTv.setVisibility(View.VISIBLE);
                            nextAppointmentTimeTv.setVisibility(View.VISIBLE);
                            cheque_typeSpinner.setVisibility(View.GONE);
                            chequetype_tv.setVisibility(View.GONE);
                            cheque_no_et.setVisibility(View.GONE);
                            chequedate_tv.setVisibility(View.GONE);
                            bankname_et.setVisibility(View.GONE);
                            amount_et.setVisibility(View.GONE);
                            reasonSpinner.setVisibility(View.GONE);
                            tv_reason.setVisibility(View.GONE);
                            tv_reason.setText("FE_REASON");
                            payment_modeSpinner.setVisibility(View.GONE);
                            tv_paymentMode.setVisibility(View.GONE);
                            signImageBytes = "";
                            verifierImageBytes = "";
                            officeImageBytes = "";
                        } else if (selectedItem.equalsIgnoreCase("SHORT ADDRESS")) {
                            etContactPersonNumber.setVisibility(View.GONE);
                            etContactPersonName.setVisibility(View.GONE);
                        } else if (selectedItem.equalsIgnoreCase("NO SERVICE AREA")) {
                            etContactPersonNumber.setVisibility(View.GONE);
                            etContactPersonName.setVisibility(View.GONE);
                        } else if (selectedItem.equalsIgnoreCase("Select")) {
                            etContactPersonNumber.setVisibility(View.GONE);
                            etContactPersonName.setVisibility(View.GONE);
                        } else {

                            nextAppointmentTv.setVisibility(View.GONE);
                            nextAppointmentDateTextTv.setVisibility(View.GONE);
                            nextAppointmentTimeTv.setVisibility(View.GONE);
                            reasonSpinner.setVisibility(View.GONE);
                            tv_reason.setVisibility(View.GONE);
                            payment_modeSpinner.setVisibility(View.GONE);
                            tv_paymentMode.setVisibility(View.GONE);
                            etContactPersonNumber.setVisibility(View.VISIBLE);
                            etContactPersonName.setVisibility(View.VISIBLE);
                        }


                        if (dataType.equalsIgnoreCase("REF")) {
                            if (selectedItem.equals("PAID")) {
                                nextAppointmentTv.setVisibility(View.GONE);
                                nextAppointmentDateTextTv.setVisibility(View.GONE);
                                nextAppointmentTimeTv.setVisibility(View.GONE);
                            } else {
                                nextAppointmentTv.setVisibility(View.VISIBLE);
                                nextAppointmentDateTextTv.setVisibility(View.VISIBLE);
                                nextAppointmentTimeTv.setVisibility(View.VISIBLE);
                            }

                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


        payment_modeSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        String selectedItem = parent
                                .getItemAtPosition(position).toString();
                        if (selectedItem.equals("CHEQUE") || selectedItem.equals("DD")) {
                            chequedate_tv.setVisibility(View.VISIBLE);
                            cheque_no_et.setVisibility(View.VISIBLE);
                            // amount_et.setVisibility(View.VISIBLE);
                        } else if (selectedItem.equals("CASH")) {
                            chequedate_tv.setVisibility(View.GONE);
                            cheque_no_et.setVisibility(View.GONE);
                            // amount_et.setVisibility(View.VISIBLE);
                        } else if (selectedItem.equals("ONLINE") || selectedItem.equals("NEFT")) {
                            chequedate_tv.setVisibility(View.GONE);
                            cheque_no_et.setVisibility(View.VISIBLE);
                            cheque_no_et.setHint("Reference Number");
                            // amount_et.setVisibility(View.VISIBLE);
                        } else {
                            chequedate_tv.setVisibility(View.GONE);
                            // amount_et.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub

                    }
                });


        pdaTypeSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        String selectedItem = parent
                                .getItemAtPosition(position).toString();
                        if (selectedItem.equals("VISIT")) {
                            contactStatusSpinner.setVisibility(View.VISIBLE);
                            tvContactStatus.setVisibility(View.VISIBLE);
                        } else {
                            contactStatusSpinner.setVisibility(View.GONE);
                            tvContactStatus.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub

                    }
                });


        cheque_typeSpinner
                .setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        String selectedItem = parent
                                .getItemAtPosition(position).toString();
                        if (selectedItem.equals("PDC")) {
                            cheque_no_et.setVisibility(View.VISIBLE);
                            chequedate_tv.setVisibility(View.VISIBLE);
                            bankname_et.setVisibility(View.VISIBLE);
                            amount_et.setVisibility(View.VISIBLE);
                        } else if (selectedItem.equals("CURRENT DATE")) {
                            cheque_no_et.setVisibility(View.GONE);
                            chequedate_tv.setVisibility(View.GONE);
                            bankname_et.setVisibility(View.GONE);
                            amount_et.setVisibility(View.GONE);
                        } else {
                            cheque_no_et.setVisibility(View.GONE);
                            chequedate_tv.setVisibility(View.GONE);
                            bankname_et.setVisibility(View.GONE);
                            amount_et.setVisibility(View.GONE);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub

                    }
                });


        chequedate_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog(chequedate_tv);
            }
        });


        verifierSignImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(mContext, CaptureSignatureActivity.class);
                startActivityForResult(intent, SIGNATURE_ACTIVITY);
            }
        });
        verifierImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                imageType = 2;
                xui.createImagePickOptionDialog();
                picLatLongstr = tracker.getLatitude() + "," + tracker.getLongitude();
            }
        });
        officeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imageType = 3;
                xui.createImagePickOptionDialog();
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // TODO Auto-generated method stub
                if (validate()) {
                    enteredValues = checkEnteredData();

                    if (utils.isNetworkAvailable(mContext)) {
                        Hashtable<String, String> ht = new Hashtable<String, String>();
                        String[] keys = Urls.collectionFormKeys;
                        for (int i = 0; i < keys.length; i++) {
                            ht.put(keys[i], enteredValues[i] + "");
                        }
                        new NetworkTask(mContext, ht, true)
                                .execute(Urls.submitCollectionOfficeForm);
                        ht = null;
                    } else {
                        try {
                            int mLength = enteredValues.length;
                            enteredValues[mLength - 3] = "SavedSignImage";
                            enteredValues[mLength - 2] = "SavedUserImage";
                            enteredValues[mLength - 1] = "SavedPlaceImage";
                            BackupItem mItem = new BackupItem();
                            mItem.setUserId(utils.getStringData("LoginId"));
                            mItem.setAvcvId(AVCVDetailsID);
                            mItem.setAvcvData(utils
                                    .convertArrayToString(enteredValues));
                            mItem.setType("office");
                            mItem.setSignImage(signImageBlobData);
                            mItem.setUserImage(userImageBlobData);
                            mItem.setPlaceImage(placeImageBlobData);
                            db.insertIntoBackUp(mItem);
                            db.deleteFromCases(AVCVDetailsID);
                            finish();
                            utils.showCenterToast(MConfig.internetMessage
                                    + "\nYour data has been saved in local storage!!");

                        } catch (Exception ee) {
                            utils.showCenterToast("Unable to save data" + "\n"
                                    + ee.getMessage());
                        }
                    }

                }
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:

                // set time picker as current time
                return new TimePickerDialog(this, timePickerListener, hour, minute,
                        false);

        }
        return null;
    }

    // Used to convert 24hr format to 12hr format with AM/PM values
    private void updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        nextAppointmentTimeTv.setText(aTime);

    }

    void checkForGPS() {
        tracker.getLocation(this);
        if (tracker.getLatitude() == 0.0) {
            if (utils.isNetworkAvailable(mContext)) {
                tracker.showSettingsAlert("Error",
                        "Please check your GPS and Internet Settings :)");
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get longitude and latitude due to internet connection !!");
            }
        } else {
            if (utils.isNetworkAvailable(mContext)) {
                new AddressTask(this).execute(tracker.getLatitude(),
                        tracker.getLongitude());
            } else {
                if (checkGPSStatus)
                    utils.showCenterToast("Unable to get location address due to internet connection !!");
            }
        }
        checkGPSStatus = false;
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        checkForGPS();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent result) {

        super.onActivityResult(requestCode, resultCode, result);
        ImageView resultImageView = null;
        byte[] imageArray = null;
        if (resultCode == RESULT_OK)
            if (requestCode == SIGNATURE_ACTIVITY) {
                imageArray = result.getByteArrayExtra("byteArray");
                String base64StringImage = Base64.encodeToString(imageArray, 0);
                signImageBlobData = imageArray;
                resultImageView = verifierSignImage;
                signImageBytes = base64StringImage;
            } else {
                imageArray = Crop.getImageBitmap(requestCode, resultCode,
                        result, (Activity) mContext, false);
                if (imageArray != null) {
                    String base64StringImage = Base64.encodeToString(
                            imageArray, 0);
                    switch (imageType) {
                        case 2:
                            userImageBlobData = imageArray;
                            resultImageView = verifierImage;
                            verifierImageBytes = base64StringImage;
                            break;
                        case 3:
                            placeImageBlobData = imageArray;
                            resultImageView = officeImage;
                            officeImageBytes = base64StringImage;
                            break;
                    }

                }

            }
        if (resultImageView != null) {
            resultImageView.setImageDrawable(null);
            Bitmap bmp = BitmapFactory.decodeByteArray(imageArray, 0,
                    imageArray.length);
            Bitmap mutableBitmap = bmp.copy(Bitmap.Config.ARGB_8888, true);


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateTime = sdf.format(Calendar.getInstance().getTime()); // reading local time in the system

            Canvas cs = new Canvas(mutableBitmap);
            Paint tPaint = new Paint();
            tPaint.setTextSize(35);
            tPaint.setColor(Color.RED);
            tPaint.setStyle(Paint.Style.FILL);
            cs.drawBitmap(mutableBitmap, 0f, 0f, null);
            float height = tPaint.measureText("yY");
            cs.drawText(dateTime, 20f, height + 15f, tPaint);
            resultImageView.setImageBitmap(mutableBitmap);
        }
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        xui = null;
        utils = null;
        valid = null;
        db = null;
        if (tracker != null) {
            tracker.stopUsingGPS();
        }
    }

    @Override
    public void onAddressComplete(String address) {
        // TODO Auto-generated method stub
        if (address != null) {
            mLocationAddress = address;
            if (picLatLongstr != null)
                picLocationstr = address;

        }
    }

    @Override
    public void onNetworkComplete(String result, String type) {
        // TODO Auto-generated method stub
        String[] resultArray = new MParser(mContext).parseXMLResponse(result);
        if (Boolean.parseBoolean(resultArray[0])) {
            db.deleteFromCases(AVCVDetailsID);
            /*Intent mIntent = new Intent(mContext, MainActivity.class);
            startActivity(mIntent);
            finishAffinity();*/
            finish();
           /* startActivity(new Intent(mContext, DownloadedCasesActivity.class));
            finish();*/
        }
        utils.showCenterToast(resultArray[1]);
    }

    private boolean validate() {
        boolean isValid = true;
        if (fe_statusSpinner.isShown()) {
            if (fe_statusSpinner.getSelectedItem().toString() == null
                    || fe_statusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Fe Reason ");
                isValid = false;
            } else {
                DispositionBean feReasonBean = (DispositionBean) fe_statusSpinner.getSelectedItem();
                fe_statusstr = feReasonBean.getFE_REASON1();
            }

        } else {

            fe_statusstr = "";
        }

        if (reason2Spinner.isShown()) {
            if (reason2Spinner.getSelectedItem().toString() == null
                    || reason2Spinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select FE Reason2 ");
                isValid = false;
            } else
                feReason2 = reason2Spinner.getSelectedItem().toString();
        } else {

            feReason2 = "";
        }


        if (pdaTypeSpinner.isShown()) {
            if (pdaTypeSpinner.getSelectedItem().toString() == null
                    || pdaTypeSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select PDA Type ");
                isValid = false;
            } else
                pdaTypestr = pdaTypeSpinner.getSelectedItem().toString();
        } else {

            pdaTypestr = "";
        }

        if (contactStatusSpinner.isShown()) {
            if (contactStatusSpinner.getSelectedItem().toString() == null
                    || contactStatusSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select Contact Status ");
                isValid = false;
            } else
                contactStatusStr = contactStatusSpinner.getSelectedItem().toString();
        } else {

            contactStatusStr = "";
        }
        if (nextAppointmentTv.isShown()) {
            if (nextAppointmentTv.getText().toString() == null
                    || nextAppointmentTv.getText().toString().length() <= 0
                    || nextAppointmentTv.getText().toString() == "") {
                utils.showCenterToast("Please enter next appointment date ");
                isValid = false;
            } else if (nextAppointmentTimeTv.getText().toString() == null
                    || nextAppointmentTimeTv.getText().toString() == ""
                    || nextAppointmentTimeTv.getText().toString().length() <= 0) {
                utils.showCenterToast("Please enter next appointment time  ");
                isValid = false;
            } else {
                String minuString = "";
                String hourString = "";
                if (minute < 10)
                    minuString = "0" + minute;
                else
                    minuString = String.valueOf(minute);
                if (hour < 10)
                    hourString = "0" + hour;
                else
                    hourString = String.valueOf(hour);

                nextAppString = nextAppointmentTv.getText().toString()
                        + hourString + minuString;
            }
        } else {
            nextAppString = "";
        }

      /*  if (etContactPersonName.isShown()) {
            if (etContactPersonName.getText().toString() == null
                    || etContactPersonName.getText().toString().length() <= 0
                    || etContactPersonName.getText().toString() == "") {
                utils.showCenterToast("Please enter contact person name ");
                isValid = false;
            } else
                contactPersonNameStr = etContactPersonName.getText().toString();
        } else {
            contactPersonNameStr = "";
        }
        if (etContactPersonNumber.isShown()) {
            if (etContactPersonNumber.getText().toString() == null
                    || etContactPersonNumber.getText().toString().length() <= 0
                    || etContactPersonNumber.getText().toString() == "") {
                utils.showCenterToast("Please enter contact person number ");
                isValid = false;
            } else
                contactPersonNumberStr = etContactPersonNumber.getText().toString();
        } else {
            contactPersonNumberStr = "";
        }
*/
        if (tc_remarksEditText.isShown()) {
            if (tc_remarksEditText.getText().toString() == null
                    || tc_remarksEditText.getText().toString().length() <= 0
                    || tc_remarksEditText.getText().toString() == "") {
                utils.showCenterToast("Please enter remarks ");
                isValid = false;
            } else
                tc_remartstr = tc_remarksEditText.getText().toString();
        } else {
            tc_remartstr = "";
        }

        if (tcfeedback_et.isShown()) {
            if (tcfeedback_et.getText().toString() == null
                    || tcfeedback_et.getText().toString().length() <= 0
                    || tcfeedback_et.getText().toString() == "") {
                utils.showCenterToast("Please enter TC Feedback ");
                isValid = false;
            } else
                tcFeedback_str = tcfeedback_et.getText().toString();
        } else {
            tcFeedback_str = "N/A";
        }

        if (chequedate_tv.isShown()) {
            if (chequedate_tv.getText().toString() == null
                    || chequedate_tv.getText().toString().length() <= 0
                    || chequedate_tv.getText().toString() == "") {
                utils.showCenterToast("Please enter cheque date ");
                isValid = false;
            } else
                chequedatestr = chequedate_tv.getText().toString();
        } else {
            chequedatestr = "";

        }
        if (cheque_no_et.isShown()) {
            if (cheque_no_et.getText().toString() == null
                    || cheque_no_et.getText().toString().length() <= 0
                    || cheque_no_et.getText().toString() == "") {
                utils.showCenterToast("Please enter cheque number ");
                isValid = false;
            } else
                cheque_no_str = cheque_no_et.getText().toString();
        } else {
            cheque_no_str = "";

        }
        if (amount_et.isShown()) {
            if (amount_et.getText().toString() == null
                    || amount_et.getText().toString().length() <= 0
                    || amount_et.getText().toString() == "") {
                utils.showCenterToast("Please enter cheque amount ");
                isValid = false;
            } else
                amountstr = amount_et.getText().toString();
        } else {
            amountstr = "";

        }
        if (bankname_et.isShown()) {
            if (bankname_et.getText().toString() == null
                    || bankname_et.getText().toString().length() <= 0
                    || bankname_et.getText().toString() == "") {
                utils.showCenterToast("Please enter Bank name ");
                isValid = false;
            } else
                banknamestr = bankname_et.getText().toString();
        } else {
            banknamestr = "";

        }
        if (cheque_typeSpinner.isShown()) {
            if (cheque_typeSpinner.getSelectedItem().toString() == null
                    || cheque_typeSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select cheque type ");
                isValid = false;
            } else
                cheque_typestr = cheque_typeSpinner.getSelectedItem().toString();
        } else {

            cheque_typestr = "";
        }

        if (payment_modeSpinner.isShown()) {
            if (payment_modeSpinner.getSelectedItem().toString() == null
                    || payment_modeSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select payment mode ");
                isValid = false;
            } else
                paymentModestr = payment_modeSpinner.getSelectedItem().toString();
        } else {

            paymentModestr = "";
        }
      /*  if (reasonSpinner.isShown()) {
            if (reasonSpinner.getSelectedItem().toString() == null
                    || reasonSpinner.getSelectedItem().toString()
                    .equals("Select")) {
                utils.showCenterToast("Please select reason ");
                isValid = false;
            } else
                fe_reasonstr = reasonSpinner.getSelectedItem().toString();
        } else {

            fe_reasonstr = "";
        }*/
//		if (!valid.isValidImage(signImageBytes, "Verifier Sign Image")
//				|| !valid.isValidImage(verifierImageBytes, "Verifier Image")
//				|| !valid.isValidImage(officeImageBytes, "Office Image")) {
//			isValid = false;
//		}


        return isValid;
    }

}
