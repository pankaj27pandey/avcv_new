
package com.offshoot.avcv.listadapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.offshoot.avcv.R;
import com.offshoot.avcv.bean.CaseItem;

public class CaseListBaseAdapter extends BaseAdapter {
	private static ArrayList<CaseItem> itemArrayList;
	private LayoutInflater layoutInflator;
	Context mContext;

	public CaseListBaseAdapter(Context context, ArrayList<CaseItem> results) {
		itemArrayList = results;
		mContext = context;
		layoutInflator = LayoutInflater.from(context);
	}

	static class ViewHolder {
		TextView itemValue;
		TextView itemName;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return itemArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	String[] mFields = { "MobileNo", "CustomerName", "AlternateNo",
			"Address", "Landmark", "City", "District", "PINCode" };

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		if (convertView == null) {
			convertView = layoutInflator.inflate(R.layout.row_case_item,
					parent, false);
			holder = new ViewHolder();
			holder.itemValue = (TextView) convertView
					.findViewById(R.id.itemValue);
			holder.itemName = (TextView) convertView
					.findViewById(R.id.itemName);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		CaseItem item = itemArrayList.get(position);
		String mAddress = item.getAVAddress();
		if(mAddress.length() > 21){
			mAddress = mAddress.substring(0, 21);
			mAddress = mAddress+"..";
		}
		String mValues = item.getMobileNo() + "\n" + item.getCustomerName()
				+ "\n" + item.getAlternateNo() + "\n" + mAddress
				+ "\n" + item.getAVLandmark() + "\n" + item.getAVCity() + "\n"
				+ item.getAVDistrict() + "\n" + item.getAVPINCode();

		holder.itemValue.setText(mValues);
		String mName = "";
		for (int i = 0; i < mFields.length; i++) {
			if (i == (mFields.length - 1)) {
				mName = mName + mFields[i];
			} else {
				mName = mName + mFields[i] + "\n";
			}
		}
		holder.itemName.setText(mName);

		return convertView;
	}

}
