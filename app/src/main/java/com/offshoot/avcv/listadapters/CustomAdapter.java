package com.offshoot.avcv.listadapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.offshoot.avcv.R;
import com.offshoot.avcv.bean.DispositionBean;

import java.util.ArrayList;

/***** Adapter class extends with ArrayAdapter ******/
public class CustomAdapter extends ArrayAdapter<String> {

    private Context mContext;
    private ArrayList data;
    public Resources res;
    DispositionBean tempValues = null;
    LayoutInflater inflater;

    /*************  CustomAdapter Constructor *****************/
    public CustomAdapter(
            Context context,
            int textViewResourceId,
            ArrayList objects,
            Resources resLocal
    ) {
        super(context, textViewResourceId, objects);

        mContext = context;
        data = objects;
        res = resLocal;

        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        View row = inflater.inflate(R.layout.spinner_row, parent, false);

        tempValues = null;
        tempValues = (DispositionBean) data.get(position);

        TextView label = row.findViewById(R.id.tvReason);


        if (position == 0) {

            // Default selected Spinner item
            label.setText("Select");
        } else {
            // Set values for spinner each row
            label.setText(tempValues.getFE_REASON1());
        }

        return row;
    }
}

