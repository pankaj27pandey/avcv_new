package com.apzone.android.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import androidx.fragment.app.Fragment;

import java.util.Hashtable;

public class NetworkTask extends AsyncTask<String, String, String> {

	Context mContext;
	NetworkTaskCompleteListener mListener;
	Hashtable<String, String> mHashTable;
	Boolean mProgress = true;
	String mURL;

	public NetworkTask(Context context, Hashtable<String, String> hashTable,
			boolean progress) {
		mContext = context;
		mListener = (NetworkTaskCompleteListener) context;
		mHashTable = hashTable;
		mProgress = progress;

	}

	ProgressDialog pd;

	public NetworkTask(Fragment fragment, Hashtable<String, String> ht, boolean progress) {
		mContext=fragment.getContext();
		mListener = (NetworkTaskCompleteListener) fragment;
		mHashTable = ht;
		mProgress = progress;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		if (mProgress) {
			pd = ProgressDialog.show(mContext, null, "Loading");
		}
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		mURL = params[0];
		String result = null;
		try {
			result = new NetworkCall(mContext).postDataToRestAPI(
					mHashTable, params[0]);
		} catch (Exception ee) {
			ee.printStackTrace();

		}
		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (mProgress) {
			pd.dismiss();
		}
		mListener.onNetworkComplete(result, mURL);
	}

}
