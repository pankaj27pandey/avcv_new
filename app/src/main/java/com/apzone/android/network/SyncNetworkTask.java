package com.apzone.android.network;

import java.util.Hashtable;
import java.util.List;

import com.apzone.android.common.CommonUtils;
import com.apzone.android.common.Urls;
import com.offshoot.avcv.db.BackupItem;
import com.offshoot.avcv.db.DbHandler;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

public class SyncNetworkTask extends AsyncTask<String, String, String> {

	Context mContext;
	NetworkTaskCompleteListener mListener;
	Boolean mProgress = true;
	String mURL;
	List<BackupItem> backupItem;
	CommonUtils utils;
	DbHandler db;

	public SyncNetworkTask(Context context, List<BackupItem> mbackupItem,
			boolean progress) {
		mContext = context;
		mListener = (NetworkTaskCompleteListener) context;
		mProgress = progress;
		backupItem = mbackupItem;
		utils = new CommonUtils(mContext);
		db = new DbHandler(mContext);
	}

	ProgressDialog pd;

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		if (mProgress) {
			pd = ProgressDialog.show(mContext, null, "Sync please wait :)");
		}
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String locationData = params[0];
		String locationAddress = params[1];
		String result = null;

		for (int k = 0; k < backupItem.size(); k++) {

			String[] mSyncValues = utils.convertStringToArray(backupItem.get(k)
					.getAvcvData());
			String mType = backupItem.get(k).getType();
			String AVCVSyncID = backupItem.get(k).getAvcvId();
			String[] keys = {};
			int mLength = mSyncValues.length;
			if (mType.equals("office") || mType.equals("resident")) {
				mSyncValues[mSyncValues.length - 8] = locationData;
				mSyncValues[mSyncValues.length - 4] = locationAddress;
				mSyncValues[mLength - 3] = Base64.encodeToString(backupItem
						.get(k).getSignImage(), 0);
				mSyncValues[mLength - 2] = Base64.encodeToString(backupItem
						.get(k).getUserImage(), 0);
				mSyncValues[mLength - 1] = Base64.encodeToString(backupItem
						.get(k).getPlaceImage(), 0);
				if (mType.equals("office")) {
					keys = Urls.OfficeFormkeys;
					mURL = Urls.submitOfficeForm;
				} else {
					keys = Urls.ResidentFormkeys;
					mURL = Urls.submitResidentForm;
				}
			} else if (mType.equals("untrace")) {
				mSyncValues[mSyncValues.length - 4] = locationData;
				mSyncValues[mSyncValues.length - 3] = locationAddress;
				mSyncValues[mLength - 2] = Base64.encodeToString(backupItem
						.get(k).getUserImage(), 0);
				mSyncValues[mLength - 1] = Base64.encodeToString(backupItem
						.get(k).getPlaceImage(), 0);
				keys = Urls.untraceableFormkeys;
				mURL = Urls.submitUntraceableUrl;
			} else {
				keys = Urls.wrongAllocationkeys;
				mURL = Urls.submitVisitStatusUrl;
			}
			Hashtable<String, String> ht = new Hashtable<String, String>();
			for (int i = 0; i < keys.length; i++) {
				ht.put(keys[i], mSyncValues[i] + "");
			}
			try {
				result = new NetworkCall(mContext).postDataToRestAPI(ht,
						mURL);
			} catch (Exception ee) {
				result = null;
			}
			if (result != null) {
				Log.e("SYNC PROCESS RESULT", result);
				db.deleteFromBackUp(AVCVSyncID);
			}
			ht = null;
		}
		return result;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		if (mProgress) {
			pd.dismiss();
		}
		mListener.onNetworkComplete(result, mURL);
	}

}
