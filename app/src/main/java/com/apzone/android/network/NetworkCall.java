package com.apzone.android.network;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.content.Context;
import android.net.http.HttpAuthHeader;
import android.util.Base64;
import android.util.Log;

import com.offshoot.avcv.mconfig.MConfig;

public class NetworkCall {

    Context mContext;

    public NetworkCall(Context context) {
        mContext = context;
    }

    public String postDataToRestAPI(Hashtable<String, String> hashTable,
                                    String url) throws Exception {
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(httpclient.getParams(),
                MConfig.timeOut);


        HttpPost httppost = new HttpPost(url);

        httppost.setHeader("WSUName", "finagevc");
        httppost.setHeader("WSUPass", "F!9@geVE#753");


        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        Enumeration<String> enumer = hashTable.keys();
        while (enumer.hasMoreElements()) {
            String key = (String) enumer.nextElement();
            nameValuePairs.add(new BasicNameValuePair(key, hashTable.get(key)));
        }
        enumer = null;
        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        Log.d("request", httppost.toString());

        // Execute HTTP Post Request
        HttpResponse httpResponse = httpclient.execute(httppost);
        HttpEntity resEntity = httpResponse.getEntity();

        String responseData = null;
        if (resEntity != null) {
            responseData = EntityUtils.toString(resEntity);
            resEntity.consumeContent();
            Log.d("RESPONSE " + url, responseData);
        }

        httpclient.getConnectionManager().shutdown();
        nameValuePairs = null;
        httpclient = null;
        httppost = null;
        return responseData;
    }

    // Get SOAP Action Name
    public String getSoapAction(String method) {
        return "\"" + MConfig.SOAPNameSpace + method + "\"";
    }

    public String postDataToSOAPService(Hashtable<String, String> hashTable,
                                        String methodName) throws Exception {
        SoapObject request = new SoapObject(MConfig.SOAPNameSpace, methodName);
        Enumeration<String> enumer = hashTable.keys();
        while (enumer.hasMoreElements()) {
            String key = (String) enumer.nextElement();
            request.addProperty(key, hashTable.get(key));
        }
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);
        HttpTransportSE androidHttpTransport = new HttpTransportSE(
                MConfig.SOAPUrl, MConfig.timeOut);
        androidHttpTransport.call(getSoapAction(methodName), envelope);
        SoapObject result = (SoapObject) envelope.bodyIn;
        request = null;
        envelope = null;
        androidHttpTransport = null;
        return result.toString();
    }
}
