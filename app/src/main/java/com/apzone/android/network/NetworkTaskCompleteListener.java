package com.apzone.android.network;

public interface NetworkTaskCompleteListener {
	public void onNetworkComplete(String result, String type);
}