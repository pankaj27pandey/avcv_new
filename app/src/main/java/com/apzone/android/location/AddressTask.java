package com.apzone.android.location;

import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

public class AddressTask extends AsyncTask<Double, String, String> {

	Context mContext;
	AddressTaskCompleteListener mListener;
	Geocoder geocoder;
	List<Address> addresses;

	public AddressTask(Context context) {
		mContext = context;
		mListener = (AddressTaskCompleteListener) context;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		mListener.onAddressComplete(result);
	}

	@Override
	protected String doInBackground(Double... params) {
		String result = null;
		try {
			geocoder = new Geocoder(mContext, Locale.getDefault());
			addresses = geocoder.getFromLocation(params[0], params[1], 1);

			String address = addresses.get(0).getAddressLine(0);
			String city = addresses.get(0).getAddressLine(1);
			String country = addresses.get(0).getAddressLine(2);
			if (addresses.get(0).getAddressLine(3) != null) {
				country = country + ", " + addresses.get(0).getAddressLine(3);
			}
			result = address + ", " + city + ", " + country;
		} catch (Exception ee) {
			result = ee.getMessage();
		}
		return result;

	}

}
