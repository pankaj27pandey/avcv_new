package com.apzone.android.location;

public interface AddressTaskCompleteListener {
	public void onAddressComplete(String address);
}