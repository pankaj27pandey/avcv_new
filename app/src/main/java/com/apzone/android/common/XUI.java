package com.apzone.android.common;

import com.apzone.android.crop.Crop;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class XUI {
	Context mContext;
	Activity activity;

	public XUI(Context context) {
		mContext = context;
		activity = (Activity) context;
	};

	DialogInterface.OnClickListener imagePickerActionListener = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch (which) {
			case 0: // Delete

				Crop.pickImage((Activity) mContext, 0);
				break;
			case 1: // Copy
				Crop.pickImage((Activity) mContext, 1);
				break;
			}
		}
	};

	public void createImagePickOptionDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setTitle("Choose an Option");
		String[] options = { "Camera", "Gallery" };
		builder.setItems(options, imagePickerActionListener);
		builder.setNegativeButton("Cancel", null);
		builder.create().show();
	}

	public void showErrorDialig() {
		new AlertDialog.Builder(mContext)
				.setTitle("Error Message")
				.setMessage(
						"This build has been expired. Please contact Developer or Apzone Technologies.\nThank you :)")
				.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// continue with delete
								dialog.dismiss();
								activity.finish();
							}
						}).setIcon(android.R.drawable.ic_dialog_alert).show();
	}

}
