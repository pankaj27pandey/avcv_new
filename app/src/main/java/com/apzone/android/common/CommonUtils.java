package com.apzone.android.common;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.apzone.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;

@SuppressLint("DefaultLocale")
public class CommonUtils {
	Context mContext;
	Context mActivity;
	SharedPreferences sharedPreferences;
	Editor editor;

	public CommonUtils(Context context) {
		mContext = context;
		mActivity =  context;
	};

	public boolean isNetworkAvailable(Context mContext) {
		ConnectivityManager cm = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public Bitmap getItemImageBitmap(String title) {
		title = title.toLowerCase();
		title = title.replace(" ", "_");
		int imgId = mContext.getResources().getIdentifier(
				mContext.getPackageName() + ":drawable/image_" + title
						+ "_icon", null, null);
		Bitmap imageBitmap = BitmapFactory.decodeResource(
				mContext.getResources(), imgId);
		return imageBitmap;
	};

	public void setStringData(String key, String value) {
		sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		editor = sharedPreferences.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public String getStringData(String key) {
		sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		return sharedPreferences.getString(key, "null");
	};

	public void setIntData(String key, int value) {
		sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		editor = sharedPreferences.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public int getIntData(String key) {
		sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		return sharedPreferences.getInt(key, -1);
	};

	public void setBoolData(String key, Boolean value) {
		sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		editor = sharedPreferences.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public Boolean getBoolData(String key) {
		sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(mContext);
		return sharedPreferences.getBoolean(key, false);
	};

	public void showCenterToast(String message) {
		Toast toast = Toast.makeText(mContext, message + "", Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	};

	public byte[] imageToByteArray(Uri localUri) {
		int rotation = 0;
		try {
			ExifInterface ei = new ExifInterface(localUri.getPath());
			int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotation = 90;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotation = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotation = 270;
				break;
			}
		} catch (Exception ee) {
		}
		Bitmap bitmapOrg = BitmapFactory.decodeFile(localUri.getPath());
		int width = bitmapOrg.getWidth();
		int height = bitmapOrg.getHeight();
		int newWidth = 300;
		int newHeight = 300;

		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		matrix.postRotate(rotation);

		Bitmap resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width,
				height, matrix, true);

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
		byte[] byteArray = stream.toByteArray();
		new File(localUri.getPath()).delete();
		return byteArray;
	};

	private void beginCrop(Uri source) {
		Uri outputUri = Crop.getOutputMediaFileUri(1);
		new Crop(source).output(outputUri).asSquare().start((Activity) mActivity);
	}

	private byte[] handleCrop(int resultCode, Intent result) {
		if (resultCode == Activity.RESULT_OK) {
			return imageToByteArray(Crop.getOutput(result));
		} else if (resultCode == Crop.RESULT_ERROR) {
			Toast.makeText(mActivity, Crop.getError(result).getMessage(),
					Toast.LENGTH_SHORT).show();
		}
		return null;
	}

	Uri tempCameraSourceURI;

	public byte[] cropAndGetImageBitmap(int requestCode, int resultCode,
			Intent result) {
		if (requestCode == Crop.REQUEST_PICK
				&& resultCode == Activity.RESULT_OK) {
			beginCrop(result.getData());
		} else if (requestCode == Crop.REQUEST_PICK_CAMERA
				&& resultCode == Activity.RESULT_OK) {
			beginCrop(Crop.getOutputMediaFileUri(0));
			tempCameraSourceURI = Crop.getOutputMediaFileUri(0);
		} else if (requestCode == Crop.REQUEST_CROP) {
			try {
				new File(tempCameraSourceURI.getPath()).delete();
			} catch (Exception ee) {
			}
			return handleCrop(resultCode, result);
		} else if (requestCode == Crop.REQUEST_SIGN && resultCode == 1111) {
			return imageToByteArray(Crop.getOutputMediaFileUri(0));
		}
		return null;
	}

	public String strSeparator = "#__#,#__#";

	public String convertArrayToString(String[] array) {
		String str = "";
		for (int i = 0; i < array.length; i++) {
			str = str + array[i];
			// Do not append comma at the end of last element
			if (i < array.length - 1) {
				str = str + strSeparator;
			}
		}
		return str;
	}

	public String[] convertStringToArray(String str) {
		String[] arr = str.split(strSeparator);
		return arr;
	}

	long validationDays = 7 * 24 * 60 * 60 * 1000;

	public Boolean isValidBuild() {
		File myFile = new File(Environment.getExternalStorageDirectory(),
				"android1_data3.txt");
		try {
			if (myFile.exists()) {
				Long lastmodified = myFile.lastModified();
				long diff = System.currentTimeMillis() - lastmodified;
				if (diff > validationDays) {
					return false;
				}
			} else {
				myFile.createNewFile();
			}

		} catch (Exception ee) {
			return false;
		}
		return true;
	}

	public static boolean hasPermissions(Context context, String... permissions) {
		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
			for (String permission : permissions) {
				if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
					return false;
				}
			}
		}
		return true;
	}
}
