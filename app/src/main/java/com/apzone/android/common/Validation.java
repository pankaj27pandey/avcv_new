package com.apzone.android.common;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

public class Validation {
	Context mContext;

	public Validation(Context context) {
		mContext = context;
	}

	ScrollView mScrollview = null;

	public Validation(Context context, ScrollView scrollview) {
		mContext = context;
		mScrollview = scrollview;
	}

	Spanned invalidMessage = Html.fromHtml("<font color='red'>Invalid</font>");
	Spanned nullMessage = Html
			.fromHtml("<font color='red'>Cannot be Null</font>");
	Spanned spaceMessage = Html
			.fromHtml("<font color='red'>Cannot starts with space</font>");
	Spanned numericMessage = Html
			.fromHtml("<font color='red'>Cannot be numeric</font>");
	Spanned notMatchMessage = Html
			.fromHtml("<font color='red'>Not Match</font>");

	// Check Email Validation
	public boolean isValidEmail(EditText edt) {
		String value = edt.getText().toString();
		if (value == null || value == "") {
			edt.setError(nullMessage);
			return false;
		} else if (value.startsWith(" ")) {
			edt.setError(spaceMessage);
			return false;
		} else if (value.length() <= 0) {
			edt.setError(nullMessage);
			return false;
		} else {
			if (!android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches()) {
				edt.setError(invalidMessage);
				return false;
			}
			return true;
		}
	}

	public boolean isValidName(EditText edt) {
		String value = edt.getText().toString();
		if (value == null || value == "") {
			edt.setError(nullMessage);
			return false;
		} else if (value.startsWith(" ")) {
			edt.setError(spaceMessage);
			return false;
		} else if (value.length() <= 0) {
			edt.setError(nullMessage);
			return false;
		} else if (!value.matches("[a-zA-Z ]+")) {
			edt.setError(numericMessage);
			return false;
		} else {
			return true;
		}
	}

	@SuppressLint("NewApi")
	public boolean isValidField(EditText edt) {
		String value = edt.getText().toString();
		boolean status = false;
		if (value == null || value == "") {
			edt.setError(nullMessage);
			status = false;
		} else if (value.startsWith(" ")) {
			edt.setError(spaceMessage);
			status = false;
		} else if (value.length() <= 0) {
			edt.setError(nullMessage);
			status = false;
		} else {
			status = true;
		}

		if (!status) {
			if (mScrollview != null) {

			}

		}
		return status;
	}

	@SuppressLint("NewApi")
	public boolean isValidSpinner(Spinner spinner, String spinnerName) {
		String value = spinner.getSelectedItem().toString();
		boolean status = false;
		if (value.equals("Select")) {
			status = false;
		} else {
			status = true;
		}
		if (!status) {
			Toast.makeText(mContext, "Please select " + spinnerName,
					Toast.LENGTH_LONG).show();
			if (mScrollview != null) {

			}
		}
		return status;
	}

	@SuppressLint("NewApi")
	public boolean isValidImage(String imageBytes, String imageName) {
		boolean status = false;
		if (imageBytes == null || imageBytes.equals("null")
				|| imageBytes.equals("")) {
			status = false;
		} else {
			status = true;
		}
		if (!status) {
			Toast.makeText(mContext, "Please upload " + imageName,
					Toast.LENGTH_LONG).show();
		}
		return status;
	}

	public boolean isMatch(EditText edt1, EditText edt2) {
		String value1 = edt1.getText().toString();
		String value2 = edt2.getText().toString();
		if (!value1.matches(value2)) {
			edt2.setError(notMatchMessage);
			return false;
		}
		return true;
	};
}
