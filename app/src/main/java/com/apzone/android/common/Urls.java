package com.apzone.android.common;

import com.offshoot.avcv.mconfig.MConfig;

public class Urls {
    // Login Url
    public static String loginUrl = MConfig.serverUrl
            + "SignInUser";

    public static String attendanceUrl = MConfig.serverUrl
            + "Submit_FEAttendance";

    public static String allocatedCaseUrl = MConfig.serverUrl
            + "Get_Allocation";


    public static String dispositionUrl = MConfig.serverUrl
            + "Get_Dispositions";


    public static String submitUntraceableUrl = MConfig.serverUrl
            + "Submit_Verification_Untraceable";

    public static String submitVisitStatusUrl = MConfig.serverUrl
            + "Submit_Verification_WA";

    public static String submitOfficeForm = MConfig.serverUrl
            + "Submit_Verification_Office";

    public static String submitResidentForm = MConfig.serverUrl
            + "Submit_Verification_Residence";
    public static String submitSalesForm = MConfig.serverUrl
            + "Submit_Sales";

    public static  String dashboardUrl(String name){

        return  "http://sigmamumbai.com/userAndrdForms/frmHome.aspx?LoginID="+name;
    }


    public static String[] salesFormkeys = {"LoginID",
            "VisitStatus", "VisitType", "CompanyName",
            "PersonMet", "ContactNo", "Desiganation", "FollowupDateTime", "Remarks", "LatLong", "LLLocation"};

    public static String[] untraceableFormkeys = {"LoginID", "VerificationID",
            "UntraceReason", "LatLong", "LLLocation",
            "Base64Customer", "Base64House"};

    public static String[] wrongAllocationkeys = {"LoginID", "VerificationID"};
    public static String[] collectionWrongAllocationkeys = {"CollectionID"};

    public static String[] OfficeFormkeys = {"LoginID", "VerificationID", "Accessibility",
            "PersonMetwith", "Designationofthepersonmet",
            "EmploymentDetails", "NatureofBusiness", "Designation",
            "OfficeStatus", "BusinessOfficeBoardSeen", "BusinessOfficeSetup",
            "WorkingSince", "NetsalaryNetprofit", "Neighbour1Name",
            "Neighbour1Status", "Neighbour2Name", "Neighbour2Status",
            "VerifierName", "Roadmap_Landmark", "MeterStatus", "AVCVStatus", "AVCVReason",
            "AVCVRemarks", "LatLong", "LLLocation",
            /*"Base64Signature"*/ "Base64Img1", "Base64Img2", "Base64Img3", "Base64Img4", "Base64Img5", "Base64Img6"};

    public static String[] ResidentFormkeys = {"LoginID", "VerificationID",
            "Accessibility", "TypeofHouse", "StandardofLiving",
            "PersonMetwith", "Relations", "Ownership", "RentPaid",
            "Accomodation", "StayingSince", "MaritalStatus", "NoofFamilyMembers",
            "EarningMembers", "PurposeofPurchase", "ProductDelivered",
            "EmploymentDetails", "NatureofBusiness", "Designation",
            "WorkingSince", "NetsalaryNetprofit", "Neighbour1Name",
            "Neighbour1Status", "Neighbour2Name", "Neighbour2Status", "VerifierName",
            "Roadmap_Landmark", "MeterStatus", "AVCVStatus", "AVCVReason", "AVCVRemarks",
            "LatLong", "LLLocation",
            "Base64Signature", "Base64Customer", "Base64House1",
            "Base64House2", "Base64House3", "Base64House4", "Base64House5"};

    //collection
    public static String paymentModeUrl = MConfig.serverUrl
            + "GetPaymentMode";

    public static String submitCollectionUntraceableUrl = MConfig.serverUrl
            + "Submit_Collection_Untraceable";

    public static String submitCollectionWAUrl = MConfig.serverUrl
            + "Submit_Collection_WA";

    public static String submitCollectionOfficeForm = MConfig.serverUrl
            + "Submit_Collection_Traceable";

    public static String[] untraceableCollectionFormkeys = {"CollectionID", "FE_STATUS",
            "FE_REASON1", "FE_REASON2", "FE_REMARKS", "LatLong", "LLLocation", "Base64_1", "Base64_2", "Base64_3"};


    public static String[] collectionFormKeys = {"CollectionID",
            "FE_STATUS", "FE_REASON1", "FE_REASON2", "FE_REMARKS", "FOLLOWUP_TIME", "FE_TYPE", "PaymentMode", "PaymentAmount", "ChequeType", "ChequeNo",
            "ChequeDate", "ChequeBank", "TCFeedback", "ContactStatus", "ContactPersonName", "ContactPersonNo", "LatLong", "LLLocation", "PICLatLong", "PICLocation",
            "Base64Signature", "Base64Customer", "Base64House"};
}
