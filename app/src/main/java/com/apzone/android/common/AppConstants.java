package com.apzone.android.common;


public class AppConstants {
    public static final int SIGNATURE_ACTIVITY = 100;
    public static String TAG="fragment";
    public static final int DB_VERSION = 1;

    public static String traceType="TRACEABLE";
    public static String unTraceType="UNTRACEABLE";
    public static final String dbName = "AVCV_FinageDataBase";

}
